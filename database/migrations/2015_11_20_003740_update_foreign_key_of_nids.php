<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateForeignKeyOfNids extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('nids', function (Blueprint $table) {
            $table->dropForeign('nids_member_id_foreign');
            $table->foreign('member_id')->references('id')->on('members')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('nids', function (Blueprint $table) {
            $table->dropForeign('nids_member_id_foreign');
            $table->foreign('member_id')->references('id')->on('members')->onUpdate('cascade')->onDelete('set null');
        });
    }
}
