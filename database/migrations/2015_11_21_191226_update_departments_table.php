<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateDepartmentsTable extends Migration
{
    public function up()
    {
        Schema::table('departments', function (Blueprint $table) {
            $table->integer('college_id')->nullable()->unsigned();
            $table->foreign('college_id')->references('id')->on('colleges')
                ->onUpdate('cascade')->onDelete('set null');
        });
    }

    public function down()
    {
        Schema::table('departments', function (Blueprint $table) {
            $table->dropForeign('departments_college_id_foreign');
            $table->dropColumn('college_id');
        });
    }
}
