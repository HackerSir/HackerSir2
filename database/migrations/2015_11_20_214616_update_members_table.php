<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('members', function (Blueprint $table) {
            $table->dropColumn('department')->nullable();
            $table->dropColumn('grade')->nullable();

            $table->integer('department_id')->nullable()->unsigned()->after('interest');
            $table->foreign('department_id')->references('id')->on('departments')
                ->onUpdate('cascade')->onDelete('set null');

            $table->integer('grade_id')->nullable()->unsigned()->after('department_id');
            $table->foreign('grade_id')->references('id')->on('grades')
                ->onUpdate('cascade')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('members', function (Blueprint $table) {
            $table->dropForeign('members_department_id_foreign');
            $table->dropForeign('members_grade_id_foreign');
            $table->dropColumn('department_id');
            $table->dropColumn('grade_id');

            $table->string('department', 20)->after('interest');
            $table->string('grade', 20)->after('department');
        });
    }
}
