<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddGradeData extends Migration
{
    protected static $grades = [
        91 => '已畢業',
        92 => '非在學',
    ];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        foreach (self::$grades as $key => $value) {
            DB::table('grades')->insert([
                'name' => $value,
                'order' => $key
            ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('grades')->orWhereIn('name', array_values(static::$grades))->delete();
    }
}
