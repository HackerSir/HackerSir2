<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCollegesTable extends Migration
{
    public function up()
    {
        Schema::create('colleges', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('order')->unique();
        });
    }

    public function down()
    {
        Schema::drop('colleges');
    }
}
