<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGradeData extends Migration
{
    protected static $grades = [
        1 => '大一',
        2 => '大二',
        3 => '大三',
        4 => '大四',
        5 => '大五',
        6 => '大六',
        7 => '大七',
        11 => '碩一',
        12 => '碩二',
        13 => '碩三',
        14 => '碩四',
        15 => '碩五',
        16 => '碩六',
        17 => '碩七',
        21 => '博一',
        22 => '博二',
        23 => '博三',
        24 => '博四',
        25 => '博五',
        26 => '博六',
        27 => '博七',
    ];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        foreach (self::$grades as $key => $value) {
            DB::table('grades')->insert([
                'name' => $value,
                'order' => $key
            ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('grades')->orWhereIn('name', array_values(static::$grades))->delete();
    }
}
