<?php

use App\User;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ConvertUserMail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //轉換信箱驗證資料
        $users = User::all();
        foreach ($users as $user) {
            DB::table('user_mails')->insert([
                'user_id' => $user->id,
                'email' => $user->email,
                'confirm_code' => $user->confirm_code,
                'confirm_at' => $user->confirm_at
            ]);
        }
        //移除驗證相關欄位
        Schema::table('users', function ($table) {
            $table->dropColumn('confirm_code');
            $table->dropColumn('confirm_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //新增驗證相關欄位
        Schema::table('users', function ($table) {
            $table->string('confirm_code', 64)->after('remember_token');
            $table->timestamp('confirm_at')->nullable()->after('confirm_code');
        });
        //轉換信箱驗證資料
        $userMails = DB::table('user_mails')->get();
        foreach ($userMails as $userMail) {
            DB::table('users')->where('email', '=', $userMail->email)->update([
                'confirm_code' => $userMail->confirm_code,
                'confirm_at' => $userMail->confirm_at
            ]);
        }
        DB::table('user_mails')->delete();
    }
}
