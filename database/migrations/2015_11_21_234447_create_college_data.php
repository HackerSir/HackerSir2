<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCollegeData extends Migration
{
    protected static $colleges = [
        1 => '商學院',
        2 => '工學院',
        3 => '建設學院',
        4 => '金融學院',
        5 => '經營管理學院',
        6 => '國際科技與管理學院',
        7 => '資電學院',
        8 => '人文社會學院',
        9 => '理學院',
    ];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        foreach (self::$colleges as $key => $value) {
            DB::table('colleges')->insert([
                'name' => $value,
                'order' => $key
            ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('colleges')->orWhereIn('name', array_values(static::$colleges))->delete();
    }
}
