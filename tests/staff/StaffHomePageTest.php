<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class StaffHomePageTest extends StaffTestCase
{
    /**
     * A basic functional test example.
     *
     * @return void
     */
    public function testStaffHomePage()
    {
        $this->visit('/')->see(trans('staff.index.title'));
    }
}
