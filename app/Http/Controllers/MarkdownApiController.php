<?php namespace App\Http\Controllers;

use App\Helper\MarkdownHelper;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class MarkdownApiController extends Controller
{
    public function markdownPreview(Request $request)
    {
        //只接受Ajax請求
        if (!$request->ajax()) {
            return "error";
        }
        $data = $request->getContent();
        //檢查是否有內容
        if (empty($data)) {
            return Response::make(" ");
        }
        return Response::make(MarkdownHelper::translate($data));
    }
}
