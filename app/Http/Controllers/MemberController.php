<?php

namespace App\Http\Controllers;

use App\Helper\LogHelper;
use App\Member;
use App\NID;
use App\User;
use App\UserMail;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class MemberController extends Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->middleware('role:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //取得社員清單
        $amountPerPage = 50;
        //搜尋
        $memberQuery = Member::query();
        if (Input::has('q')) {
            $q = Input::get('q');
            //模糊匹配
            $q = '%' . $q . '%';
            //搜尋：本名、暱稱、信箱、手機、科系、年級、NID
            $memberQuery->where(function ($query) use ($q) {
                $query->where('realname', 'like', $q)
                    ->orWhere('nickname', 'like', $q)
                    ->orWhere('email', 'like', $q)
                    ->orWhere('phone', 'like', $q)
                    ->orWhere('department', 'like', $q)
                    ->orWhere('grade', 'like', $q)
                    ->orWhereExists(function ($query) use ($q) {
                        $query->select(DB::raw(1))
                            ->from('nids')
                            ->where('nid', 'like', $q)
                            ->whereRaw('nids.member_id = members.id');
                    });
            });
        }
        $totalCount = $memberQuery->count();
        $memberList = $memberQuery->paginate($amountPerPage);
        return view('member.list')
            ->with('memberList', $memberList)
            ->with('amountPerPage', $amountPerPage)
            ->with('totalCount', $totalCount);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('member.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nid' => 'required|max:20',
            'realname' => 'required|max:20',
            'nickname' => 'max:20',
            'gender' => 'max:20',
            'email' => 'email|max:255',
            'phone' => 'max:20',
            'interest' => 'max:65535',
            'department' => 'exists:departments,id',
            'grade' => 'exists:grades,id',
            'info' => 'max:65535'
        ]);
        if ($validator->fails()) {
            return Redirect::route('member.create')
                ->withErrors($validator)
                ->withInput();
        }
        $nidStr = strtoupper($request->get('nid'));

        if (NID::find($nidStr)) {
            return Redirect::route('member.create')
                ->withInput()
                ->with('warning', '該NID已被使用');
        }
        $member = Member::create([
            'realname' => $request->get('realname'),
            'nickname' => $request->get('nickname'),
            'gender' => $request->get('gender'),
            'email' => $request->get('email'),
            'phone' => $request->get('phone'),
            'interest' => $request->get('interest'),
            'department_id' => ($request->has('department')) ? $request->get('department') : null,
            'grade_id' => ($request->has('grade')) ? $request->get('grade') : null,
            'info' => $request->get('info')
        ]);

        NID::create([
            'nid' => $nidStr,
            'member_id' => $member->id
        ]);

        //檢查並更新會員與社員的連結
        self::updateLink($nidStr);

        //紀錄
        LogHelper::info(
            '[MemberCreated] ' . Auth::user()->email . ' 新增了社員(Id: ' . $member->id
            . ', Realname: ' . $member->realname . ')',
            $member
        );

        return Redirect::route('member.show', $member->id)
            ->with('global', '社員已新增');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $member = Member::find($id);
        if (!$member) {
            return Redirect::route('member.index')
                ->with('warning', '社員不存在');
        }
        return view('member.show')->with('member', $member);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $member = Member::find($id);
        if (!$member) {
            return Redirect::route('member.index')
                ->with('warning', '社員不存在');
        }
        return view('member.edit')->with('member', $member);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $member = Member::find($id);
        if (!$member) {
            return Redirect::route('member.index')
                ->with('warning', '社員不存在');
        }
        $validator = Validator::make($request->all(), [
            'realname' => 'required|max:20',
            'nickname' => 'max:20',
            'gender' => 'max:20',
            'email' => 'email|max:255',
            'phone' => 'max:20',
            'interest' => 'max:65535',
            'department' => 'exists:departments,id',
            'grade' => 'exists:grades,id',
            'info' => 'max:65535'
        ]);
        if ($validator->fails()) {
            return Redirect::route('member.create')
                ->withErrors($validator)
                ->withInput();
        }
        //複製一份，在Log時比較差異
        $beforeEdit = $member->replicate();

        $member->realname = $request->get('realname');
        $member->nickname = $request->get('nickname');
        $member->gender = $request->get('gender');
        $member->email = $request->get('email');
        $member->phone = $request->get('phone');
        $member->interest = $request->get('interest');
        $member->department_id = ($request->has('department')) ? $request->get('department') : null;
        $member->grade_id = ($request->has('grade')) ? $request->get('grade') : null;
        $member->info = $request->get('info');

        $member->save();
        $afterEdit = $member->replicate();
        //紀錄
        LogHelper::info(
            '[MemberEdited] ' . Auth::user()->email . ' 編輯了社員(Id: ' . $member->id
            . ', Realname: ' . $member->realname . ')',
            "編輯前",
            $beforeEdit,
            "編輯後",
            $afterEdit
        );

        return Redirect::route('member.show', $member->id)
            ->with('global', '社員已儲存');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $member = Member::find($id);
        if (!$member) {
            return Redirect::route('member.index')
                ->with('warning', '社員不存在');
        }
        //紀錄
        LogHelper::info(
            '[MemberDeleted] ' . Auth::user()->email . ' 刪除了社員(Id: ' . $member->id
            . ', Realname: ' . $member->realname . ')',
            $member
        );
        $member->delete();
        return Redirect::route('member.index')
            ->with('global', '社員已刪除');
    }

    public function addNID(Request $request, $id)
    {
        $member = Member::find($id);
        if (!$member) {
            return Redirect::route('member.index')
                ->with('warning', '社員不存在');
        }
        $validator = Validator::make($request->all(), [
            'nid' => 'required|max:20'
        ]);
        if ($validator->fails()) {
            return Redirect::route('member.show', $member->id)
                ->withErrors($validator)
                ->withInput();
        }
        $nidStr = strtoupper($request->get('nid'));

        if (NID::find($nidStr)) {
            return Redirect::route('member.show', $member->id)
                ->withInput()
                ->with('warning', '該NID已被使用');
        }

        NID::create([
            'nid' => $nidStr,
            'member_id' => $member->id
        ]);

        //檢查並更新會員與社員的連結
        self::updateLink($nidStr);

        return Redirect::route('member.show', $member->id)
            ->with('global', 'NID已新增');
    }

    public function removeNID(Request $request, $id)
    {
        $member = Member::find($id);
        if (!$member) {
            return Redirect::route('member.index')
                ->with('warning', '社員不存在');
        }
        $nidStr = strtoupper($request->get('nid'));
        $nid = NID::find($nidStr);
        if (!$nid) {
            return Redirect::route('member.show', $member->id)
                ->with('warning', 'NID不存在');
        }
        $nid->delete();

        //檢查並更新會員與社員的連結
        self::updateLink($nidStr);

        return Redirect::route('member.show', $member->id)
            ->with('global', 'NID已刪除');
    }

    public function getCheckLink()
    {
        $nid = Input::get('nid');
        $user = null;
        $member = null;
        //檢查有無指定NID
        if ($nid) {
            //尋找社員資料
            $nidObject = NID::find($nid);
            if ($nidObject) {
                $member = $nidObject->member;
            }
            //尋找會員資料
            $searchMail = $nid . '@fcu.edu.tw';
            $userMail = UserMail::where('email', '=', $searchMail)->whereNotNull('confirm_at')->first();
            if ($userMail) {
                $user = $userMail->user;
            }
        }
        return view('member.check-link')->with('nid', $nid)->with('user', $user)->with('member', $member);
    }

    public function postUpdateLink(Request $request)
    {
        $nid = $request->get('nid');
        if (!self::updateLink($nid)) {
            return Redirect::route('member.check-link', ['nid' => $nid])
                ->with('warning', '連結狀態沒有改變');
        }
        return Redirect::route('member.check-link', ['nid' => $nid])
            ->with('global', '連結已更新');
    }

    public static function updateLink($nid)
    {
        //FIXME: 邏輯需要重新最佳化
        //尋找社員資料
        $nidObject = NID::find($nid);
        $searchMail = $nid . '@fcu.edu.tw';
        if (!$nidObject) {
            //不存在的NID不該有連結
            $userMail = UserMail::where('email', '=', $searchMail)->whereNotNull('confirm_at')->first();
            if ($userMail) {
                if ($userMail->user->member) {
                    $delete = true;
                    foreach ($userMail->user->member->NIDs as $nidItem) {
                        foreach ($userMail->user->userMails as $userMailItem) {
                            if (strcasecmp($userMailItem->email, $nidItem->nid . '@fcu.edu.tw')) {
                                $delete = false;
                                break;
                            }
                        }
                        if (!$delete) {
                            break;
                        }
                    }
                    if ($delete) {
                        $userMail->user->member->user_id = null;
                        $userMail->user->member->save();
                        return true;
                    }
                }
            }
            return false;
        }
        $member = $nidObject->member;
        //尋找會員資料
        if ($member->user) {
            //該NID已連結
            //檢查連結是否該存在
            $userMail = UserMail::where('user_id', '=', $member->user->id)
                ->where('email', '=', $searchMail)->whereNotNull('confirm_at')->first();
            if (!$userMail) {
                //不該存在的連結
                $member->user_id = null;
                $member->save();
                return true;
            }
            return false;
        }
        //若無連結，則根據信箱尋找
        $userMail = UserMail::where('email', '=', $searchMail)->whereNotNull('confirm_at')->first();
        if (!$userMail) {
            return false;
        }
        if ($userMail->user->member) {
            //已連結其他社員
            return false;
        }
        //建立連結
        $member->user_id = $userMail->user->id;
        $member->save();
        return true;
    }
}
