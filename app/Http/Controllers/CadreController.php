<?php

namespace App\Http\Controllers;

use App\Cadre;
use App\Helper\LogHelper;
use App\Member;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class CadreController extends Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->middleware('role:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $uid = Input::get('uid');
        $member = Member::find($uid);
        if (!$member) {
            return Redirect::route('member.index')
                ->with('warning', '社員不存在');
        }
        return view('cadre.create')->with('member', $member);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|max:255',
            'start_at' => 'date',
            'end_at' => 'date',
            'comment' => 'max:65535'
        ]);
        $uid = $request->get('uid');
        $member = Member::find($uid);
        if (!$member) {
            return Redirect::route('member.index')
                ->with('warning', '社員不存在');
        }
        if ($validator->fails()) {
            return Redirect::route('cadre.create', ['uid' => $uid])
                ->withErrors($validator)
                ->withInput();
        }
        //檢查時間
        $startAt = ($request->has('start_at')) ? $request->get('start_at') : null;
        $endAt = ($request->has('end_at')) ? $request->get('end_at') : null;
        if ($endAt != null) {
            if ($startAt == null) {
                $endAt = null;
            } else {
                if ((new Carbon($startAt))->gte(new Carbon($endAt))) {
                    $endAt = null;
                }
            }
        }
        //新增資料
        $cadre = Cadre::create([
            'member_id' => $member->id,
            'title' => $request->get('title'),
            'start_at' => $startAt,
            'end_at' => $endAt,
            'comment' => $request->get('comment')
        ]);
        //記錄
        LogHelper::info(
            '[CadreCreated] ' . Auth::user()->email . ' 新增了社員的職務(Id: ' . $member->id
            . ', Realname: ' . $member->realname . ', Title: ' . $cadre->title . ')',
            $member,
            $cadre->attributesToArray()
        );

        return Redirect::route('member.show', $member->id)
            ->with('global', '職務已新增');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        $cadre = Cadre::find($id);
        if (!$cadre) {
            return Redirect::route('member.index')
                ->with('warning', '職務記錄不存在');
        }
        return view('cadre.edit')->with('cadre', $cadre);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $cadre = Cadre::find($id);
        if (!$cadre) {
            return Redirect::route('member.index')
                ->with('warning', '職務記錄不存在');
        }
        $validator = Validator::make($request->all(), [
            'title' => 'required|max:255',
            'start_at' => 'date',
            'end_at' => 'date',
            'comment' => 'max:65535'
        ]);
        if ($validator->fails()) {
            return Redirect::route('cadre.edit', $id)
                ->withErrors($validator)
                ->withInput();
        }
        //檢查時間
        $startAt = ($request->has('start_at')) ? $request->get('start_at') : null;
        $endAt = ($request->has('end_at')) ? $request->get('end_at') : null;
        if ($endAt != null) {
            if ($startAt == null) {
                $endAt = null;
            } else {
                if ((new Carbon($startAt))->gte(new Carbon($endAt))) {
                    $endAt = null;
                }
            }
        }
        //複製一份，在Log時比較差異
        $beforeEdit = $cadre->replicate();

        $member = $cadre->member;
        //更新資料
        $cadre->title = $request->get('title');
        $cadre->start_at = $startAt;
        $cadre->end_at = $endAt;
        $cadre->comment = $request->get('comment');
        $cadre->save();

        $afterEdit = $cadre->replicate();

        //記錄
        LogHelper::info(
            '[CadreEdited] ' . Auth::user()->email . ' 修改了社員的職務(Id: ' . $member->id
            . ', Realname: ' . $member->realname . ', Title: ' . $cadre->title . ')',
            $member,
            "編輯前",
            $beforeEdit->attributesToArray(),
            "編輯後",
            $afterEdit->attributesToArray()
        );

        return Redirect::route('member.show', $member->id)
            ->with('global', '職務已修改');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        $cadre = Cadre::find($id);
        if (!$cadre) {
            return Redirect::route('member.index')
                ->with('warning', '職務記錄不存在');
        }
        $member = $cadre->member;
        //記錄
        LogHelper::info(
            '[CadreDeleted] ' . Auth::user()->email . ' 刪除了社員的職務(Id: ' . $member->id
            . ', Realname: ' . $member->realname . ', Title: ' . $cadre->title . ')',
            $member,
            $cadre->attributesToArray()
        );

        $cadre->delete();
        return Redirect::route('member.show', $member->id)
            ->with('global', '職務已刪除');
    }
}
