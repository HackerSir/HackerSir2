<?php

namespace App\Http\Controllers;

use App\Announcement;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;

class AnnouncementController extends Controller
{
    /**
     * AnnouncementController constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->middleware('role:admin', [
            'except' => [
                'index',
                'show'
            ]
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //FIXME: 排列依據需修改，若已開始時間排序，須考慮到無開始時間之公告
        $announcements = Announcement::with('user')->orderBy('id', 'desc')->paginate();
        return view('announcement.index', compact('announcements'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('announcement.create-or-edit')->with('methodText', '新增');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|max:255',
            'start_at' => 'date',
            'end_at' => 'date|after:start_at',
            'message' => 'required|max:65535'
        ]);

        $announcement = Announcement::create([
            'title' => $request->get('title'),
            'start_at' => $request->get('start_at') ?: null,
            'end_at' => $request->get('end_at') ?: null,
            'message' => $request->get('message'),
            'user_id' => Auth::user()->id
        ]);

        return redirect()->route('announcement.show', $announcement)->with('global', '公告已建立');
    }

    /**
     * Display the specified resource.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $announcement = Announcement::find($id);
        if (!$announcement) {
            return Redirect::route('announcement.index')
                ->with('warning', '公告不存在');
        }
        return view('announcement.show', compact('announcement'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $announcement = Announcement::find($id);
        if (!$announcement) {
            return Redirect::route('announcement.index')
                ->with('warning', '公告不存在');
        }
        return view('announcement.create-or-edit', compact('announcement'))->with('methodText', '修改');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $announcement = Announcement::find($id);
        if (!$announcement) {
            return Redirect::route('announcement.index')
                ->with('warning', '公告不存在');
        }
        $this->validate($request, [
            'title' => 'required|max:255',
            'start_at' => 'date',
            'end_at' => 'date|after:start_at',
            'message' => 'required|max:65535'
        ]);

        $announcement->title = $request->get('title');
        $announcement->start_at = $request->get('start_at') ?: null;
        $announcement->end_at = $request->get('end_at') ?: null;
        $announcement->message = $request->get('message');
        $announcement->save();

        return redirect()->route('announcement.show', $announcement)->with('global', '公告已更新');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $announcement = Announcement::find($id);
        if (!$announcement) {
            return Redirect::route('announcement.index')
                ->with('warning', '公告不存在');
        }
        $announcement->delete();
        return redirect()->route('announcement.index')->with('global', '公告已刪除');
    }
}
