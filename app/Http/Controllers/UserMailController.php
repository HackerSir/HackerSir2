<?php

namespace App\Http\Controllers;

use App\Helper\LogHelper;
use App\User;
use App\UserMail;
use Exception;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;

class UserMailController extends Controller
{
    public function __construct()
    {
        parent::__construct();
        //需完成信箱驗證
        $this->middleware('email');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('user-mail.list');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'email' => 'required|email'
            ]
        );

        if ($validator->fails()) {
            return Redirect::route('user-mail.index')
                ->withErrors($validator)
                ->withInput();
        }
        $user = Auth::user();
        $email = $request->get('email');
        //檢查是否存在已驗證信箱，或自己曾添加過該信箱
        $userMailCount = UserMail::where('email', '=', $email)->where(function ($query) use ($user) {
            $query->whereNotNull('confirm_at')
                ->orWhere('user_id', '=', $user->id);
        })->count();
        if ($userMailCount > 0) {
            return Redirect::route('user-mail.index')
                ->with('warning', '該信箱已被使用')
                ->withInput();
        }
        //驗證碼
        $code = str_random(60);
        //建立資料
        $userMail = UserMail::create([
            'user_id' => $user->id,
            'email' => $email,
            'confirm_code' => $code
        ]);
        if (!$userMail) {
            return Redirect::route('user-mail.index')
                ->with('warning', '新增信箱時發生錯誤。');
        }
        //Log
        LogHelper::info('[AddUserMail] 新增備用信箱' . $email, [
            'user-mail' => $user->email,
            'email' => $email,
            'ip' => $request->getClientIp()
        ]);
        //發送驗證信件
        try {
            Mail::queue(
                'emails.alternate',
                [
                    'nickname' => $userMail->user->getNickname(),
                    'link' => URL::route('user.confirm', $code)
                ],
                function ($message) use ($email) {
                    $message->to($email)->subject("[" . Config::get('site.name') . "] 信箱驗證");
                }
            );
        } catch (Exception $e) {
            //Log
            LogHelper::info('[SendEmailFailed] 無法寄出驗證信件給' . $email, [
                'email' => $email,
                'ip' => $request->getClientIp()
            ], $e->getMessage());
            //刪除郵件資料
            $userMail->delete();

            return Redirect::route('user-mail.index')
                ->with('warning', '無法寄出驗證信件，請檢查信箱是否填寫正確，或是稍後再嘗試。')
                ->withInput();
        }
        return Redirect::route('user-mail.index')
            ->with('global', '信箱已新增，請至信箱收取信件並完成驗證。');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $user = Auth::user();
        $email = $request->get('email');
        $userMail = UserMail::where('user_id', '=', $user->id)->where('email', '=', $email)->first();
        if (!$userMail) {
            return Redirect::route('user-mail.index')
                ->with('warning', '刪除信箱時發生錯誤。');
        }
        //Log
        LogHelper::info('[DeleteUserMail] 刪除備用信箱' . $email, [
            'user-mail' => $user->email,
            'email' => $email,
            'ip' => $request->getClientIp()
        ]);
        //若是MyMail
        if (ends_with($email, '@fcu.edu.tw')) {
            $nid = explode("@", $email)[0];
            //檢查並更新會員與社員的連結
            MemberController::updateLink($nid);
        }
        //刪除信箱資料
        $userMail->delete();
        return Redirect::route('user-mail.index')
            ->with('global', '信箱已刪除。');
    }
}
