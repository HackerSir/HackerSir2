<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class AboutController extends Controller
{
    public function getIndex()
    {
        return redirect()->route('about.club');
    }

    public function getClub()
    {
        return view('front.about.club');
    }

    public function getCadre()
    {
        return view('front.about.cadre');
    }

    public function getMilestone()
    {
        return view('front.about.milestone');
    }

    public function getRule()
    {
        return view('front.about.rule');
    }

    public function getFinancial()
    {
        return view('front.about.financial');
    }

    public function getFriend()
    {
        return view('front.about.friend');
    }

}
