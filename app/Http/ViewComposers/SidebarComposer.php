<?php namespace App\Http\ViewComposers;

use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\URL;

class SidebarComposer
{

    /**
     * The user repository implementation.
     *
     */
    protected $users;

    /**
     * Create a new profile composer.
     */
    public function __construct()
    {
        //
    }

    /**
     * Bind data to the view.
     *
     * @param  View $view
     * @return void
     */
    public function compose(View $view)
    {
        $staffSidebarConfig = Config::get('sidebar.staff');
        $sidebar = [];
        foreach ($staffSidebarConfig as $item) {
            $sidebarItem = $item;
            //當前項目
            $url = null;
            if (isset($item['route'])) {
                $url = route($item['route']);
            } elseif (isset($item['path'])) {
                $url = url($item['path']);
            }
            $sidebarItem['active'] = false;
            if ($url) {
                if (starts_with(URL::current(), $url) && $url != url('/') || URL::current() == $url) {
                    $sidebarItem['active'] = true;
                }
            }
            //額外選項
            $options = array_merge([
                'disable' => false       //此選項是否無法點擊
            ], (isset($item['option']) && is_array($item['option'])) ? $item['option'] : []);
            $sidebarItem['option'] = $options;
            $sidebar[] = $sidebarItem;
        }
        $view->with('sidebar', $sidebar);
    }

}
