<?php

namespace App\Http\Middleware;

use Closure;
use Entrust;
use Illuminate\Support\Facades\Auth;
use Menu;

class LaravelMenu
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        Menu::make('navBar', function ($menu) {
            //基本巡覽列
            if (Auth::check()) {
                //會員
                if (Entrust::hasrole('admin')) {
                    //管理員
                    $adminMenu = $menu->add('站點管理');
                    $adminMenu->add('成員清單', ['route' => 'user.list']);
                    $adminMenu->add('資料連結', ['route' => 'member.check-link']);
                }
                $userMenu = $menu->add(Auth::user()->getNickname());
                $userMenu->add('個人資料', ['route' => 'user.profile']);
                $userMenu->add('修改密碼', ['route' => 'user.change-password']);
                $userMenu->add('登出', ['route' => 'user.logout']);
            } else {
                //遊客
                $menu->add('登入', ['route' => 'user.login']);
            }
        });
        return $next($request);
    }
}
