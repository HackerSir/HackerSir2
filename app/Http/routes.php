<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
 */

// Staff (Sub-Domain for Control Panel)
Route::group(['domain' => 'staff.' . env('APP_DOMAIN', 'hackersir2.localhost')], function () {
    //Index
    Route::get('/', [
        'as' => 'staff.index',
        'uses' => 'StaffController@index',
    ]);
    //Auth System
    Route::controller('user', 'UserController', [
        'getIndex' => 'user.list',
        'getLogin' => 'user.login',
        'postLogin' => 'user.login',
        'getRegister' => 'user.register',
        'postRegister' => 'user.register',
        'getConfirm' => 'user.confirm',
        'getResend' => 'user.resend',
        'postResend' => 'user.resend',
        'getForgotPassword' => 'user.forgot-password',
        'postForgotPassword' => 'user.forgot-password',
        'getResetPassword' => 'user.reset-password',
        'postResetPassword' => 'user.reset-password',
        'getChangePassword' => 'user.change-password',
        'postChangePassword' => 'user.change-password',
        'getProfile' => 'user.profile',
        'getEditProfile' => 'user.edit-profile',
        'postEditProfile' => 'user.edit-profile',
        'getEditOtherProfile' => 'user.edit-other-profile',
        'postEditOtherProfile' => 'user.edit-other-profile',
        'getLogout' => 'user.logout'
    ]);

    //UserMail
    Route::resource('user-mail', 'UserMailController', ['only' => ['index', 'store', 'destroy']]);

    // Member
    Route::post('member/addNID/{id}', [
        'as' => 'member.add-nid',
        'uses' => 'MemberController@addNID'
    ]);
    Route::delete('member/removeNID/{id}', [
        'as' => 'member.remove-nid',
        'uses' => 'MemberController@removeNID'
    ]);
    Route::get('member/check-link', [
        'as' => 'member.check-link',
        'uses' => 'MemberController@getCheckLink'
    ]);
    Route::post('member/update-link', [
        'as' => 'member.update-link',
        'uses' => 'MemberController@postUpdateLink'
    ]);
    Route::resource('member', 'MemberController');


    // Setting
//    Route::controller('setting', 'SettingController', array(
//        'getIndex' => 'setting.index',
//
//    ));

    // Issue
    Route::controller('issue', 'IssueController', [
        'getIndex' => 'issue.index',

    ]);

    // Agenda
//    Route::controller('agenda', 'CalendarController', array(
//        'getIndex' => 'agenda.index',
//    ));

    // Document
    Route::resource('document', 'DocumentController');

    // Financial
    //Route::resource('financial', 'FinancialController');

    // Cadre
    Route::resource('cadre', 'CadreController', ['except' => ['index', 'show']]);

    // Announcement
    Route::resource('announcement', 'AnnouncementController');

    //Log Viewer
    Route::get('log', [
        'as' => 'log',
        'uses' => '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index',
        'middleware' => 'role:admin'
    ]);

    // Undefined
    Route::get('{all}', [
        'as' => 'not-found',
        function () {
            abort(404);
        }
    ])->where('all', '.*');
});

// Index (for guest)
Route::get('/', [
    'as' => 'home',
    'uses' => 'HomeController@index'
]);

// Announce
Route::resource('announce', 'EnterPageController', ['only' => ['index', 'show']]);

// About
Route::controller('about', 'AboutController', [
    'getIndex' => 'about.index',
    'getClub' => 'about.club',
    'getCadre' => 'about.cadre',
    'getMilestone' => 'about.milestone',
    'getRule' => 'about.rule',
    'getFinancial' => 'about.financial',
    'getFriend' => 'about.friend',
]);

// Lesson
Route::group(['prefix' => 'lesson'], function () {
    Route::get('regular', [
        'as' => 'lesson.regular',
        function () {
            return view('front.lesson.regular');
        }
    ]);
    Route::get('cooperation', [
        'as' => 'lesson.cooperation',
        function () {
            return view('front.lesson.cooperation');
        }
    ]);
    Route::get('special', [
        'as' => 'lesson.special',
        function () {
            return view('front.lesson.special');
        }
    ]);
});

// Activity
Route::resource('activity', 'ActivityController', ['only' => ['index', 'show']]);

// Policies
Route::group(['prefix' => 'policies'], function () {
    // Terms
    Route::get('terms', [
        'as' => 'policies.terms',
        function () {
            return view('policies.terms');
        }
    ]);
    // Privacy
    Route::get('privacy', [
        'as' => 'policies.privacy',
        function () {
            return view('policies.privacy');
        }
    ]);
});

//Markdown API
Route::any('markdown', [
    'as' => 'markdown.preview',
    'uses' => 'MarkdownApiController@markdownPreview'
]);

// Undefined
Route::get('{all}', [
    'as' => 'not-found',
    function () {
        abort(404);
    }
])->where('all', '.*');
