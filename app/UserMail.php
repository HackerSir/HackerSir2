<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserMail extends BaseModel
{
    protected $table = 'user_mails';
    protected $fillable = ['user_id', 'email', 'confirm_code', 'confirm_at'];
    protected $primaryKey = ['user_id', 'email'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function isConfirmed()
    {
        if (!empty($this->confirm_at)) {
            return true;
        }
        return false;
    }

    public function isPrimary()
    {
        return $this->user->email == $this->email;
    }
}
