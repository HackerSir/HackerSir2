<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NID extends Model
{
    protected $table = 'nids';
    protected $fillable = ['nid', 'member_id'];
    protected $primaryKey = 'nid';

    public function member()
    {
        return $this->belongsTo('App\Member');
    }
}
