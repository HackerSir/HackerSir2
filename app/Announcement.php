<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * 公告
 *
 * @property integer id
 * @property string title
 * @property string message
 * @property \Carbon\Carbon|null start_at
 * @property \Carbon\Carbon|null end_at
 * @property integer|null user_id
 *
 * @property-read User|null user
 *
 * @property \Carbon\Carbon|null created_at
 * @property \Carbon\Carbon|null updated_at
 * @property \Carbon\Carbon|null deleted_at
 */
class Announcement extends Model
{
    //使用軟刪除
    use SoftDeletes;

    /** @var string $table 資料表 */
    protected $table = 'announcements';

    /** @var array $fillable 可大量指派的屬性 */
    protected $fillable = [
        'title',
        'message',
        'start_at',
        'end_at',
        'user_id'
    ];

    /** @var array $dates 自動轉換為Carbon的屬性 */
    protected $dates = ['start_at', 'end_at'];

    /** @var integer 分頁時的每頁數量 */
    protected $perPage = 20;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
