<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Zizaco\Entrust\Traits\EntrustUserTrait;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract
{
    use Authenticatable, CanResetPassword;
    use EntrustUserTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email',
        'nickname',
        'password',
        'register_ip',
        'register_at',
        'lastlogin_ip',
        'lastlogin_at'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    public function member()
    {
        return $this->hasOne('App\Member');
    }

    public function userMails()
    {
        return $this->hasMany('App\UserMail')->orderBy('created_at');
    }

    public function primaryMail()
    {
        return $this->hasOne('App\UserMail')->where('user_id', '=', $this->id)->where('email', '=', $this->email);
    }

    public function isConfirmed()
    {
        if ($this->primaryMail) {
            if ($this->primaryMail->isConfirmed()) {
                return true;
            }
        }
        return false;
    }

    public function getNickname()
    {
        if (!empty($this->nickname)) {
            $nickname = $this->nickname;
        } else {
            $nickname = explode("@", $this->email)[0];
        }
        return $nickname;
    }
}
