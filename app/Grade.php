<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Grade extends Model
{
    public $timestamps = false;
    protected $table = 'grades';
    protected $fillable = ['name', 'order'];

    public static function selectArray()
    {
        $gradeList = static::orderBy('order')->get();
        $gradeArray = [null => '-- 請下拉選擇年級 --'];
        foreach ($gradeList as $grade) {
            $gradeArray[$grade->id] = $grade->name;
        }
        return $gradeArray;
    }

    public function members()
    {
        return $this->hasMany('App\Member');
    }
}
