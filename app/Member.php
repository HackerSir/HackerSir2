<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
    protected $table = 'members';
    protected $fillable = ['realname', 'nickname', 'gender', 'email', 'phone', 'interest', 'department_id', 'grade_id', 'info'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function NIDs()
    {
        return $this->hasMany('App\NID')->orderBy('created_at');
    }

    public function cadres()
    {
        return $this->hasMany('App\Cadre')->orderBy('start_at');
    }

    public function department()
    {
        return $this->belongsTo('App\Department');
    }

    public function grade()
    {
        return $this->belongsTo('App\Grade');
    }

    public function getCadresText()
    {
        $cadresTextArray = [];
        foreach ($this->cadres as $cadre) {
            if ($cadre->isInOffice()) {
                $cadresTextArray[] = $cadre->title;
            }
        }
        return implode('、', $cadresTextArray);
    }
}
