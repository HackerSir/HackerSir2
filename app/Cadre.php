<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Cadre extends Model
{
    protected $table = 'cadres';
    protected $fillable = ['member_id', 'title', 'start_at', 'end_at', 'comment'];

    public function member()
    {
        return $this->belongsTo('App\Member');
    }

    //取得任期狀態
    public function getStatus()
    {
        //未設定期限，視為在任中
        if (empty($this->start_at) && empty($this->end_at)) {
            return 'office';
        }
        //已經開始
        $hasStart = true;
        if (!empty($this->start_at)) {
            $startAt = new Carbon($this->start_at);
            if (Carbon::now()->lte($startAt)) {
                $hasStart = false;
            }
        }
        //已經結束
        $hasEnd = false;
        if (!empty($this->end_at)) {
            $endAt = new Carbon($this->end_at);
            if (Carbon::now()->gte($endAt)) {
                $hasEnd = true;
            }
        }
        if (!$hasStart) {
            return 'future';
        }
        if ($hasEnd) {
            return 'history';
        }
        return 'office';
    }

    //歷任（已卸任）
    public function isInHistory()
    {
        return $this->getStatus() == 'history';
    }

    //在任期內
    public function isInOffice()
    {
        return $this->getStatus() == 'office';
    }

    //未來擔任
    public function isInFuture()
    {
        return $this->getStatus() == 'future';
    }
}
