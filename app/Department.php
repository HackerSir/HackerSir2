<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    public $timestamps = false;
    protected $table = 'departments';
    protected $fillable = ['nickname', 'name', 'order', 'college_id'];

    //http://stackoverflow.com/questions/27098537/how-to-implement-optgroup-in-laravel-blade-templeting-engine
    public static function selectArray()
    {
        $collegesList = College::orderBy('order')->lists('name', 'id');
        $departmentsList = static::orderBy('order')->get();

        $departmentArray = [null => '-- 請下拉選擇科系 --'];

        foreach ($departmentsList as $department) {
            $value = $department->name;
            if (!empty($department->nickname)) {
                $value .= ' [' . $department->nickname . ']';
            }

            $departmentArray[$collegesList[$department->college_id]][$department->id] = $value;
        }
        return $departmentArray;
    }

    public function members()
    {
        return $this->hasMany('App\Member');
    }

    public function college()
    {
        return $this->belongsTo('App\College');
    }

    public function getNickname()
    {
        if (!$this->nickname) {
            return $this->name;
        }
        return $this->nickname;
    }

    public function getName()
    {
        $name = $this->name;
        if ($this->nickname) {
            $name .= ' [' . $this->nickname . ']';
        }
        return $name;
    }
}
