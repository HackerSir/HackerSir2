# HackerWebSite
黑客社網站 2.0
如果網站可以更好，那就開工吧！

## 網址
- Homepage  
[http://hackersir.org](http://hackersir.org)  
- Staff  
[http://staff.hackersir.org](http://staff.hackersir.org)

## 基礎架構
- PHP (PHP 5.6)
- MySQL (MariaDB 10.0)

## 主要框架
- Laravel 5.1
- Bootstrap 3

## 文件格式
- Use [PSR-2](http://www.php-fig.org/psr/psr-2/) as Coding Style
- Use [EditorConfig](http://editorconfig.org/) for other coding style settings

## 推薦IDE
- PhpStorm
