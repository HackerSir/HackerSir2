<?php

/**
 * 側邊欄
 *
 *
 * 完整格式：
 * [
 *     'text' => '連結文字',
 *     //可選
 *     'icon' => '圖標的class',
 *
 *     //三選一
 *     'route' => '連結路由名稱',
 *     'path' => '連結路徑',
 *     'url' => '連結網址',
 *
 *      //可選
 *     'role' => '角色檢查',
 *     'perm' => '權限檢查',
 *
 *     //額外選項
 *     'option' => [
 *         'disable' => false       //此選項無法點擊
 *     ]
 * ]
 * 注意：route、path、url擇一即可
 */

return [
    'staff' => [
        [
            'text' => '總　　覽',
            'route' => 'staff.index'
        ],
        [
            'text' => '待辦事項',
            'path' => 'issue',
            //'route' => 'issue.index'
            'icon' => 'fa fa-calendar-check-o'
        ],
        [
            'text' => '時 間 表',
            'option' => [
                'disable' => true
            ]
        ],
        [
            'text' => '文　　件',
            'route' => 'document.index'
        ],
        [
            'text' => '社員資料',
            'route' => 'member.index',
            'role' => 'admin'
        ],
        [
            'text' => '社團課程',
            'option' => [
                'disable' => true
            ]
        ],
        [
            'text' => '社團活動',
            'option' => [
                'disable' => true
            ]
        ],
        [
            'text' => '社團投票',
            'option' => [
                'disable' => true
            ]
        ],
        [
            'text' => '社網公告',
            'route' => 'announcement.index'
        ],
        [
            'text' => '記 錄 檔',
            'url' => 'log'
        ],
        [
            'text' => '黑客社舊網站',
            'url' => 'https://hackersir.info/'
        ]
    ]
];
