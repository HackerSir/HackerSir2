@extends('app')

@section('title')
    {{ trans('staff.index.title') }}
@endsection

@section('content')
    <div class="container-fluid">
        @if(Auth::check())
            <div class="row">
                <div class="col-lg-6">
                    {{-- Issue List --}}
                    <h3>任務列表</h3>

                </div>
                <div class="col-lg-6">
                    {{-- Meeting/Activity List --}}
                    <h3>近期活動</h3>

                </div>
            </div>
            <div class="row">
                <h3>近期簽到紀錄</h3>
            </div>
        @else
            <h2 class="text-center">嗨！你看起來很眼熟！</h2>
            <br>
            <p class="text-center">你是不是沒有{!! link_to_route('user.login', '登入') !!}呢？</p>
        @endif
    </div>
@endsection
