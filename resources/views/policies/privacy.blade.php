@extends('app')

@section('title')隱私權政策@endsection

@section('css')
    <style>
        h1 {
            font-size: 5em;
        }
        p#article {
            font-size: 15pt;
        }
    </style>
@endsection

@section('content')
    <div class="container">
        <div class="text-center">
            <h1>隱私權政策</h1>
            <h3><small>上次更新時間：2015年11月13日</small></h3>
        </div>
        <hr>
        <p id="article">
            當您開始使用、瀏覽逢甲大學黑客社網站時，即表示您信任我們對於您個人資訊的處理方式。本《隱私權政策》旨在協助您了解我們會收集您哪些資訊、原因及用途，這是非常重要的事項，希望您撥冗詳閱。
        </p>
    </div>
@endsection