@extends('app')

@section('title')
    新增社員
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="well bs-component">
                    {!! Form::open(['route' => 'member.store', 'class' => 'form-horizontal']) !!}
                    <fieldset>
                        <legend>新增社員</legend>
                    </fieldset>
                    <div class="form-group has-feedback{{ ($errors->has('realname'))?' has-error':'' }}">
                        <label class="control-label col-md-2" for="realname">本名</label>
                        <div class="col-md-9">
                            {!! Form::text('realname', null, ['id' => 'realname', 'placeholder' => '請輸入社員本名', 'class' => 'form-control', 'required']) !!}
                            @if($errors->has('realname'))<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>
                            <span class="label label-danger">{{ $errors->first('realname') }}</span>@endif
                        </div>
                    </div>
                    <div class="form-group has-feedback{{ ($errors->has('nid'))?' has-error':'' }}">
                        <label class="control-label col-md-2" for="nid">NID</label>
                        <div class="col-md-9">
                            {!! Form::text('nid', null, ['id' => 'nid', 'placeholder' => '請輸入社員NID', 'class' => 'form-control', 'required']) !!}
                            @if($errors->has('nid'))<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>
                            <span class="label label-danger">{{ $errors->first('nid') }}</span>@endif
                        </div>
                    </div>
                    <div class="form-group has-feedback{{ ($errors->has('nickname'))?' has-error':'' }}">
                        <label class="control-label col-md-2" for="nickname">暱稱</label>
                        <div class="col-md-9">
                            {!! Form::text('nickname', null, ['id' => 'nickname', 'placeholder' => '請輸入社員暱稱', 'class' => 'form-control']) !!}
                            @if($errors->has('nickname'))<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>
                            <span class="label label-danger">{{ $errors->first('nickname') }}</span>@endif
                        </div>
                    </div>
                    <div class="form-group has-feedback{{ ($errors->has('gender'))?' has-error':'' }}">
                        <label class="control-label col-md-2" for="gender">性別</label>
                        <div class="col-md-9">
                            {!! Form::text('gender', null, ['id' => 'gender', 'placeholder' => '請輸入社員性別', 'class' => 'form-control']) !!}
                            @if($errors->has('gender'))<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>
                            <span class="label label-danger">{{ $errors->first('gender') }}</span>@endif
                        </div>
                    </div>
                    <div class="form-group has-feedback{{ ($errors->has('email'))?' has-error':'' }}">
                        <label class="control-label col-md-2" for="email">信箱</label>
                        <div class="col-md-9">
                            {!! Form::email('email', null, ['id' => 'email', 'placeholder' => '請輸入社員信箱', 'class' => 'form-control']) !!}
                            @if($errors->has('email'))<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>
                            <span class="label label-danger">{{ $errors->first('email') }}</span>@endif
                        </div>
                    </div>
                    <div class="form-group has-feedback{{ ($errors->has('phone'))?' has-error':'' }}">
                        <label class="control-label col-md-2" for="phone">手機</label>
                        <div class="col-md-9">
                            {!! Form::text('phone', null, ['id' => 'phone', 'placeholder' => '請輸入社員手機', 'class' => 'form-control']) !!}
                            @if($errors->has('phone'))<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>
                            <span class="label label-danger">{{ $errors->first('phone') }}</span>@endif
                        </div>
                    </div>
                    <div class="form-group has-feedback{{ ($errors->has('interest'))?' has-error':'' }}">
                        <label class="control-label col-md-2" for="interest">興趣</label>
                        <div class="col-md-9">
                            {!! Form::textarea('interest', null, ['id' => 'interest', 'placeholder' => '請輸入社員興趣', 'class' => 'form-control']) !!}
                            @if($errors->has('interest'))<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>
                            <span class="label label-danger">{{ $errors->first('interest') }}</span>@endif
                        </div>
                    </div>
                    <div class="form-group has-feedback{{ ($errors->has('department'))?' has-error':'' }}">
                        <label class="control-label col-md-2" for="department">科系</label>
                        <div class="col-md-9">
                            {!! Form::select('department', \App\Department::selectArray(), null, ['class' => 'form-control']) !!}
                            @if($errors->has('department'))<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>
                            <span class="label label-danger">{{ $errors->first('department') }}</span>@endif
                        </div>
                    </div>
                    <div class="form-group has-feedback{{ ($errors->has('grade'))?' has-error':'' }}">
                        <label class="control-label col-md-2" for="grade">年級</label>
                        <div class="col-md-9">
                            {!! Form::select('grade', \App\Grade::selectArray(), null, ['class' => 'form-control']) !!}
                            @if($errors->has('grade'))<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>
                            <span class="label label-danger">{{ $errors->first('grade') }}</span>@endif
                        </div>
                    </div>
                    <div class="form-group has-feedback{{ ($errors->has('info'))?' has-error':'' }}">
                        <label class="control-label col-md-2" for="info">資訊</label>
                        <div class="col-md-9">
                            {!! Form::textarea('info', null, ['id' => 'info', 'placeholder' => '請輸入社員資訊', 'class' => 'form-control']) !!}
                            @if($errors->has('info'))<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>
                            <span class="label label-danger">{{ $errors->first('info') }}</span>@endif
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-9 col-md-offset-2">
                            {!! Form::submit('新增社員', ['class' => 'btn btn-primary']) !!}
                            {!! HTML::linkRoute('member.index', '返回', [], ['class' => 'btn btn-default']) !!}
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script type="text/javascript">
        $('select').select2();
    </script>
@endsection
