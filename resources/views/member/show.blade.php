@extends('app')

@section('title')
    {{ $member->realname }} - 社員資料
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">{{ $member->realname }} - 社員資料</div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="text-center">
                                {{-- Gravatar大頭貼 --}}
                                <img src="{{ Gravatar::src($member->email, 200) }}" class="img-circle" /><br />
                            </div>
                        </div>
                        <hr />
                        <div class="row">
                            <div class="text-center col-md-12 col-md-offset-0">
                                <table class="table table-hover">
                                    <tr>
                                        <td class="col-md-2">本名：</td>
                                        <td>{{ $member->realname }}</td>
                                    </tr>
                                    <tr>
                                        <td>會員資料：</td>
                                        <td>
                                            @if($member->user)
                                                <a href="{{ URL::route('user.profile', $member->user->id) }}">
                                                    {{ $member->user->getNickname() }}
                                                </a>
                                            @else
                                                <span class="text-danger">尚未連結</span>
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="col-md-2">職稱：<br />（任期內）</td>
                                        <td>{{ $member->getCadresText() }}</td>
                                    </tr>
                                    <tr>
                                        <td>NID：</td>
                                        <td>
                                            <a href="javascript:void(0)" title="編輯" id="toggle-nid-editable"><span class="fa fa-pencil-square-o pull-right"></span></a><br />
                                            @foreach($member->NIDs as $NID)
                                                {{ $NID->nid }}
                                                {!! Form::open(['route' => ['member.remove-nid', $member->id], 'style' => 'display: inline', 'method' => 'DELETE', 'onSubmit' => "return confirm('確定要刪除此NID嗎？');"]) !!}
                                                {!! Form::hidden('nid', $NID->nid) !!}
                                                {!! Form::submit('刪除', ['class' => 'btn btn-danger btn-xs nid-edit hidden']) !!}
                                                {!! Form::close() !!}
                                                <br />
                                            @endforeach
                                            <br />
                                            <div class="nid-edit hidden">
                                                {!! Form::open(['route' => ['member.add-nid', $member->id], 'style' => 'display: inline', 'class' => 'form-inline']) !!}
                                                <div class="form-group form-group-sm">
                                                    {!! Form::text('nid', null, ['id' => 'nid', 'placeholder' => '請輸入社員NID', 'class' => 'form-control', 'required']) !!}
                                                </div>
                                                {!! Form::submit('新增', ['class' => 'btn btn-danger btn-xs']) !!}
                                                {!! Form::close() !!}
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>暱稱：</td>
                                        <td>{{ $member->nickname }}</td>
                                    </tr>
                                    <tr>
                                        <td>性別：</td>
                                        <td>{{ $member->gender }}</td>
                                    </tr>
                                    <tr>
                                        <td>信箱：</td>
                                        <td>{{ $member->email }}</td>
                                    </tr>
                                    <tr>
                                        <td>手機：</td>
                                        <td>{{ $member->phone }}</td>
                                    </tr>
                                    <tr>
                                        <td>興趣：</td>
                                        <td>{!! nl2br(htmlspecialchars($member->interest)) !!}</td>
                                    </tr>
                                    <tr>
                                        <td>科系：</td>
                                        <td>
                                            @if($member->department)
                                                {{ $member->department->getName() }}
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>年級：</td>
                                        <td>
                                            @if($member->grade)
                                                {{ $member->grade->name }}
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>資訊：</td>
                                        <td>{!! nl2br(htmlspecialchars($member->info)) !!}</td>
                                    </tr>
                                </table>
                                <div>
                                    {!! HTML::linkRoute('member.edit', '編輯社員資料', $member->id, ['class' => 'btn btn-primary']) !!}
                                    {!! HTML::linkRoute('member.index', '返回社員列表', [], ['class' => 'btn btn-default']) !!}
                                    {!! Form::open(['route' => ['member.destroy', $member->id], 'style' => 'display: inline', 'method' => 'DELETE',
                                    'onSubmit' => "return confirm('確定要刪除社員嗎？');"]) !!}
                                    {!! Form::submit('刪除', ['class' => 'btn btn-danger']) !!}
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">擔任職務</div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="text-center col-md-12 col-md-offset-0">
                                {!! HTML::linkRoute('cadre.create', '新增', ['uid' => $member->id], ['class' => 'btn btn-primary pull-right']) !!}
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th class="text-center">職稱</th>
                                            <th class="text-center">起訖時間</th>
                                            <th class="text-center">操作</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @forelse($member->cadres as $cadre)
                                            <tr>
                                                <td>
                                                    @if($cadre->isInHistory())
                                                        <span class="fa fa-arrow-left" title="已卸任"></span>
                                                    @elseif($cadre->isInOffice())
                                                        <span class="fa fa-exchange" title="在任中"></span>
                                                    @elseif($cadre->isInFuture())
                                                        <span class="fa fa-arrow-right" title="未來將擔任"></span>
                                                    @endif
                                                    {{ $cadre->title }}
                                                    @if($cadre->comment)
                                                        <span class="fa fa-info-circle" title="{!! nl2br(htmlspecialchars($cadre->comment)) !!}"></span>
                                                    @endif
                                                </td>
                                                <td>
                                                    @if($cadre->start_at)
                                                        <span title="{{ (new Carbon($cadre->start_at))->diffForHumans() }}">{{ $cadre->start_at }}</span>
                                                    @else
                                                        很久以前
                                                    @endif
                                                    ～
                                                    @if($cadre->end_at)
                                                        <span title="{{ (new Carbon($cadre->end_at))->diffForHumans() }}">{{ $cadre->end_at }}</span>
                                                    @else
                                                        遙遠的將來
                                                    @endif
                                                </td>
                                                <td>
                                                    {!! HTML::linkRoute('cadre.edit', '編輯', $cadre->id, ['class' => 'btn btn-primary btn-sm']) !!}
                                                    {!! Form::open(['route' => ['cadre.destroy', $cadre->id], 'style' => 'display: inline', 'method' => 'DELETE',
                                                    'onSubmit' => "return confirm('確定要刪除此職務記錄嗎？');"]) !!}
                                                    {!! Form::submit('刪除', ['class' => 'btn btn-danger btn-sm']) !!}
                                                    {!! Form::close() !!}
                                                </td>
                                            </tr>
                                        @empty
                                            <tr>
                                                <td colspan="3">
                                                    未擔任任何職務
                                                </td>
                                            </tr>
                                        @endforelse
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script type="text/javascript">
        $('#toggle-nid-editable').click(function (){
            $('.nid-edit').toggleClass('hidden');
        });
    </script>
@endsection
