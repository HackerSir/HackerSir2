@extends('app')

@section('title')
    資料連結
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="well bs-component">
                    <fieldset>
                        <legend>資料連結</legend>
                    </fieldset>
                    <div>
                        {!! Form::open(['route' => 'member.check-link', 'class' => 'form-horizontal', 'method' => 'GET']) !!}
                        <div class="input-group">
                            {!! Form::text('nid', $nid, ['id' => 'nid', 'placeholder' => '請輸入NID', 'class' => 'form-control', 'required']) !!}
                            <span class="input-group-btn">
                                <button type="submit" class="btn btn-primary" title="搜尋"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
                                @if($nid)
                                    <a class="btn btn-default" href="{{ URL::current() }}" title="清除"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a>
                                @endif
                            </span>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
            @if($nid)
                <div class="col-md-8 col-md-offset-2">
                    <div class="well bs-component">
                        <div class="row">
                            <div class="col-md-5">
                                <div class="lead text-center">會員</div>
                                @if($user)
                                    <div class="text-center">
                                        {{-- Gravatar大頭貼 --}}
                                        <img src="{{ Gravatar::src($user->email, 200) }}" class="img-circle" /><br />
                                        <a href="{{ URL::route('user.profile', $user->id) }}">{{ $user->getNickname() }}<br />{{ $user->email }}</a>
                                    </div>
                                @else
                                    <div class="alert alert-danger">找不到會員</div>
                                @endif
                            </div>
                            <div class="col-md-2 text-center">
                                <div class="lead text-center">狀態</div>
                                @if($user && $member)
                                    @if($user->id == $member->user_id)
                                        <span class="text-success">已互相連結</span>
                                    @else
                                        <span class="text-danger">未互相連結</span>
                                    @endif
                                @endif
                                {!! Form::open(['route' => 'member.update-link', 'class' => 'form-horizontal']) !!}
                                {!! Form::hidden('nid', $nid) !!}
                                {!! Form::submit('更新連結', ['class' => 'btn btn-danger', 'title' => '更新對於此NID的連結']) !!}
                                {!! Form::close() !!}
                            </div>
                            <div class="col-md-5">
                                <div class="lead text-center">社員</div>
                                @if($member)
                                    <div class="text-center">
                                        {{-- Gravatar大頭貼 --}}
                                        <img src="{{ Gravatar::src($member->email, 200) }}" class="img-circle" /><br />
                                        <a href="{{ URL::route('member.show', $member->id) }}">
                                            {{ $member->realname }}
                                            @if($member->nickname)
                                                （{{ $member->nickname }}）
                                            @endif
                                            <br />
                                            {{ $member->email }}
                                        </a><br />
                                        @if($member->department)
                                            {{ $member->department->getNickname() }}
                                        @endif
                                        @if($member->grade)
                                            {{ $member->grade->name }}
                                        @endif
                                        <br />
                                        {{ $member->getCadresText() }}
                                    </div>
                                @else
                                    <div class="alert alert-danger">找不到社員</div>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            @if(!(!$user && !$member) && ($user && $user->id) != ($member && $member->user_id))
                                <div class="row text-center">
                                    <div class="col-md-5">
                                        @if($user && $user->member)
                                            <div class="lead text-center">連結社員</div>
                                            <img src="{{ Gravatar::src($user->member->email, 40) }}" class="img-circle" />
                                            <a href="{{ URL::route('member.show', $user->member->id) }}">
                                                {{ $user->member->realname }}
                                                @if($user->member->nickname)
                                                    （{{ $user->member->nickname }}）
                                                @endif
                                            </a>
                                        @endif
                                    </div>
                                    <div class="col-md-5 col-md-offset-2">
                                        @if($member && $member->user)
                                            <div class="lead text-center">連結會員</div>
                                            <img src="{{ Gravatar::src($member->user->email, 200) }}" class="img-circle" /><br />
                                            <a href="{{ URL::route('user.profile', $member->user->id) }}">{{ $member->user->getNickname() }}</a>
                                        @endif
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>
@endsection
