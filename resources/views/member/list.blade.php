@extends('app')

@section('title')
    社員清單
@endsection

@section('css')
    <style type="text/css">
        {{-- 使表格文字垂直置中 --}}
        .table > tbody > tr > td {
            vertical-align: middle;
        }
    </style>
@endsection

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">社員清單</div>
        <div class="panel-body">
            <div class="row" style="margin-bottom: 10px;">
                <div class="col-md-5">
                    {!! Form::open(['route' => 'member.index', 'class' => 'form-horizontal', 'method' => 'GET']) !!}
                    <div class="input-group">
                        {!! Form::text('q', Input::get('q'), ['id' => 'q', 'placeholder' => '搜尋本名、暱稱、信箱、手機、科系、年級、NID...', 'class' => 'form-control', 'required']) !!}
                        <span class="input-group-btn">
                                        <button type="submit" class="btn btn-primary" title="搜尋"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
                            @if(Input::has('q'))
                                <a class="btn btn-default" href="{{ URL::current() }}" title="清除搜尋結果"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a>
                            @endif
                                    </span>
                    </div>
                    {!! Form::close() !!}
                </div>
                <div class="col-md-5">
                    @if(Input::has('q'))
                        @if($memberList->count())
                            符合「{{ Input::get('q') }}」的資料共 {{ $totalCount }} 筆
                        @else
                            找不到符合「{{ Input::get('q') }}」的資料
                        @endif
                    @endif
                </div>
                <div class="col-md-2 text-right">
                    {!! link_to_route('member.create', '新增', [], ['class' => 'btn btn-primary']) !!}
                </div>
            </div>

            <table class="table table-hover table-bordered">
                <thead>
                <tr>
                    <th>#</th>
                    <th>本名（暱稱）</th>
                    <th>職稱（任期內）</th>
                    <th>NID</th>
                    <th>系級</th>
                    <th>信箱</th>
                    <th>手機</th>
                    <th>會員資料</th>
                    <th>操作</th>
                </tr>
                </thead>
                <tbody>
                @foreach($memberList as $key => $memberItem)
                    <tr>
                        <td>{{ (Input::get('page',1) - 1) * $amountPerPage + $key + 1}}</td>
                        <td>
                            <img src="{{ Gravatar::src($memberItem->email, 40) }}" class="img-circle"/>
                            <a href="{{ route('member.show',$memberItem->id) }}">
                                {{ $memberItem->realname }}
                                @if(!empty($memberItem->nickname))
                                    （{{ $memberItem->nickname }}）
                                @endif
                            </a>
                        </td>
                        <td>
                            {{ $memberItem->getCadresText() }}
                        </td>
                        <td>
                            @foreach($memberItem->NIDs as $NID)
                                {{ $NID->nid }}<br/>
                            @endforeach
                        </td>
                        <td>
                            @if($memberItem->department)
                                {{ $memberItem->department->getNickname() }}
                            @endif
                            @if($memberItem->grade)
                                {{ $memberItem->grade->name }}
                            @endif
                        </td>
                        <td>
                            {{ $memberItem->email }}
                        </td>
                        <td>
                            {{ $memberItem->phone }}
                        </td>
                        <td>
                            @if($memberItem->user)
                                <a href="{{ URL::route('user.profile', $memberItem->user->id) }}">
                                    {{ $memberItem->user->getNickname() }}
                                </a>
                            @endif
                        </td>
                        <td>
                            <a href="{{ route('member.edit', $memberItem->id) }}" title="編輯"><span class="fa fa-pencil-square-o"></span></a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="text-center">
                {!! str_replace('/?', '?', $memberList->appends(Input::except(['page']))->render()) !!}
            </div>
        </div>
    </div>
@endsection
