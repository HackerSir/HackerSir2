<!DOCTYPE html>
<html lang="zh-Hant-TW">
    <head>
        {{-- MetaTag --}}
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="{{ Config::get('site.desc') }}">

        {{-- MetaTag(OpenGraph) --}}
        <meta property="og:title"
              content="@if(trim($__env->yieldContent('title'))) @yield('title') - @endif{{ Config::get('site.name') }}">
        <meta property="og:url" content="{{ URL::current() }}">
        <meta property="og:image" content="{{ asset('img/index-header.jpg') }}">
        <meta property="og:description" content="{{ Config::get('site.desc') }}">

        {{-- Title --}}
        <title>@if (trim($__env->yieldContent('title'))) @yield('title') - @endif{{ Config::get('site.name') }}</title>

        {{-- Sheetstyle --}}
        @if(starts_with(Request::server('HTTP_HOST'), 'staff'))
            {!! HTML::style('//maxcdn.bootstrapcdn.com/bootswatch/3.3.5/flatly/bootstrap.min.css') !!}
            {!! HTML::style('//cdnjs.cloudflare.com/ajax/libs/select2/4.0.1-rc.1/css/select2.min.css') !!}
        @else
            {!! HTML::style('//maxcdn.bootstrapcdn.com/bootswatch/3.3.5/superhero/bootstrap.min.css') !!}
        @endif
        {!! HTML::style('//maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css') !!}
        {!! HTML::style('css/animate.css') !!}
        {!! HTML::style('css/octicons.css') !!}
        {!! HTML::style('css/app.css') !!}
        {!! HTML::style('css/tipped.css') !!}


        @if(starts_with(Request::server('HTTP_HOST'), 'staff'))
            {!! HTML::style('css/staff-navbar.css') !!}
            {!! HTML::style('css/dashboard.css') !!}
        @else
            {!! HTML::style('css/navbar.css') !!}
        @endif

        @yield('css')

        @yield('head-javascript')
    </head>
    <body>
        {{-- Header --}}
        @yield('header')

        {{-- Navbar --}}
        @if(starts_with(Request::server('HTTP_HOST'), 'staff'))
            @include('navbar.staff')
        @elseif(Request::is('/'))
            @include('navbar.index')
        @else
            @include('navbar.front')
        @endif

        {{-- SideBar & Content--}}
        @if(starts_with(Request::server('HTTP_HOST'), 'staff') && Auth::check())
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-2 col-sm-3 sidebar">
                        @include('sidebar.staff')
                    </div>
                    <div class="col-lg-10 col-lg-offset-2 col-sm-9 col-sm-offset-3">
                        @yield('content')
                    </div>
                </div>
            </div>
        @else
            @yield('content')
        @endif


        {{-- Footer --}}
        @if(starts_with(Request::server('HTTP_HOST'), 'staff'))
            @include('footer.staff')
        @else
            @include('footer.front')
        @endif

        {{-- Javascript --}}
        {!! HTML::script('//ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js') !!}
        {!! HTML::script('//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js') !!}
        @if(starts_with(Request::server('HTTP_HOST'), 'staff'))
            {!! HTML::script('//cdnjs.cloudflare.com/ajax/libs/select2/4.0.1-rc.1/js/select2.full.min.js') !!}
        @endif
        {{-- 好看的彈出訊息框 --}}
        {!! HTML::script('js/bootstrap-notify.min.js') !!}
        {!! HTML::script('js/notify.js') !!}
        {!! HTML::script('js/tipped.js') !!}

        @if(App::environment('production'))
            {!! HTML::script('js/analyticstracking.js') !!}
        @endif

        @yield('js')

        <script type="text/javascript">
            @if(Session::has('global'))
                /* Global message */
                notifySuccess('{!! Session::get('global') !!}');
            @endif
            @if(Session::has('warning'))
                /* Warning message */
                notifyWarning('{!! Session::get('warning') !!}', '{{ Session::get('delay', 0) }}');
            @endif

            $(document).ready(function() {
                Tipped.create('*',{
                    fadeIn: 0,
                    fadeOut: 0,
                    position: 'right',
                    target: 'mouse',
                    showDelay: 0,
                    hideDelay: 0,
                    offset: { x: 0, y: 15 },
                    stem: false
                });
            });
        </script>

    </body>
</html>
