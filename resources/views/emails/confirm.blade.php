@extends('emails.mail')

@section('content')
    <h2 style="text-align: center">嘿 {{ $nickname }}，歡迎加入{{ Config::get('site.name') }}！</h2>
    <p>
        有人向我們提出帳號申請，請透過以下連結，完成帳號驗證：<br/>
        <a href="{{ $link }}">{{ $link }}</a><br/>
        <span style="color:grey;font-size: 50%">(連結在24小時內有效，24小時後請重新申請信箱驗證)</span>
    </p>
    <p>
        如果上面的網址不是連結，請您將該網址複製到瀏覽器(IE、Firefox、Chrome等)的網址列。<br/>
        如果您沒有註冊帳號，請您忽略這封信，您不需要回覆這封信件或通知我們，這個帳號會在驗證時間過後被刪除！
    </p>
    <br/>
@endsection
