@extends('emails.mail')

@section('content')
    <h2 style="text-align: center">嘿 {{ $nickname }}，您最近有新增備用信箱嗎？</h2>
    <p>
        有人向我們申請新增備用信箱！請透過以下連結，完成備用信箱驗證：<br/>
        <a href="{{ $link }}">{{ $link }}</a><br/>
        <span style="color:grey;font-size: 50%">(連結在24小時內有效，24小時後請重新申請)</span>
    </p>
    <p>
        如果上面的網址不是連結，請您將該網址複製到瀏覽器(IE、Firefox、Chrome等)的網址列。<br/>
        如果您沒有要求新增備用信箱，請您忽略這封信，您不需要回覆這封信件或通知我們！
    </p>
    <br/>
@endsection
