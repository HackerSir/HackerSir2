@extends('app')

@section('title')
    404 - PageNotFoundException
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <h1>Not found.</h1>
            </div>
        </div>
    </div>
@endsection
