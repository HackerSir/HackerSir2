{{--
    $name: 控制項名稱
--}}
<div class="form-group has-feedback{{ ($errors->has($name))?' has-error':'' }}">
    <label class="control-label col-sm-3 col-lg-2" for="info">{{ $labelName }}</label>
    <div class="col-sm-7 col-lg-10" role="tabpanel">
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#edit" aria-controls="edit" role="tab" data-toggle="tab" id="tab_edit">編輯</a></li>
            <li role="presentation"><a href="#{{ $name }}_preview" aria-controls="{{ $name }}_preview" role="tab" data-toggle="tab" id="tab_{{ $name }}_preview">預覽</a></li>
        </ul>
    </div>
    <div class="col-sm-7 col-lg-10 col-sm-offset-5 col-lg-offset-2">
        <!-- Tab panes -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="edit">
                {!! Form::textarea($name, $text, ['id' => $name, 'placeholder' => '請輸入內容簡介', 'class' => 'form-control', 'style' => 'resize: vertical']) !!}
                <small>
                    <b>提示：</b>內容簡介支援{!! link_to('http://markdown.tw/', 'Markdown', ['target' => '_blank']) !!}語法
                </small>
            </div>
            <div role="tabpanel" class="tab-pane" id="{{ $name }}_preview" style="background-color: white; border: 1px solid #cccccc; padding: 8px 12px; min-height: 236px;">
                Loading...
            </div>
        </div>
        @if($errors->has($name))<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>
        <span class="label label-danger">{{ $errors->first($name) }}</span>@endif
    </div>
</div>
