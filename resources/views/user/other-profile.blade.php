@extends('app')

@section('title')
    {{ $showUser->getNickname() }} - 個人資料
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="panel panel-default">
                    <div class="panel-heading">{{ $showUser->getNickname() }} - 個人資料</div>
                    {{-- Panel body --}}
                    <div class="panel-body">
                        <div class="row">
                            <div class="text-center">
                                {{-- Gravatar大頭貼 --}}
                                <img src="{{ Gravatar::src($showUser->email, 200) }}" class="img-circle" /><br />
                            </div>
                        </div>
                        <hr />
                        <div class="row">
                            <div class="text-center col-md-10 col-md-offset-1">
                                <table class="table table-hover">
                                    <tr>
                                        <td>Email：</td>
                                        <td>
                                            @foreach($showUser->userMails as $userMail)
                                                {{ $userMail->email }}
                                                @if($userMail->isPrimary())
                                                    <span class="label label-info">主要</span>
                                                @endif
                                                @if($userMail->isConfirmed())
                                                    <span class="label label-success">已驗證</span>
                                                @else
                                                    <span class="label label-danger">未驗證</span>
                                                @endif
                                                <br />
                                            @endforeach
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>暱稱：</td>
                                        <td>{{ $showUser->nickname }}</td>
                                    </tr>
                                    <tr>
                                        <td>用戶組：</td>
                                        <td>
                                            @foreach($showUser->roles as $role)
                                                {{ $role->display_name }}<br />
                                            @endforeach
                                        </td>
                                    </tr>
                                    @if(Entrust::hasRole('admin'))
                                        <tr>
                                            <td colspan="2" class="danger">
                                                以下僅管理員可見
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>社員資料：</td>
                                            <td>
                                                @if($showUser->member)
                                                    <a href="{{ URL::route('member.show', $showUser->member->id) }}">
                                                        {{ $showUser->member->realname }}
                                                        @if($showUser->member->nickname)
                                                            （{{ $showUser->member->nickname }}）
                                                        @endif
                                                    </a>
                                                @else
                                                    <span class="text-danger">尚未連結</span>
                                                @endif
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>註冊時間：</td>
                                            <td>{{ $showUser->register_at }}</td>
                                        </tr>
                                        <tr>
                                            <td>註冊IP：</td>
                                            <td>{{ $showUser->register_ip }}</td>
                                        </tr>
                                        <tr>
                                            <td>最後登入時間：</td>
                                            <td>{{ $showUser->lastlogin_at }}</td>
                                        </tr>
                                        <tr>
                                            <td>最後登入IP：</td>
                                            <td>{{ $showUser->lastlogin_ip }}</td>
                                        </tr>
                                    @endif
                                </table>
                                @if(Entrust::hasRole('admin'))
                                    <div class="text-center">
                                        {!! HTML::linkRoute('user.edit-other-profile', '編輯資料', $showUser->id, ['class' => 'btn btn-primary']) !!}
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
