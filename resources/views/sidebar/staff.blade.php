<ul class="nav nav-sidebar">
    @if(isset($sidebar))
        @foreach($sidebar as $item)
            @if(!empty($item['text']))
                @if((empty($item['role']) || Entrust::hasRole($item['role'])) && (empty($item['perm']) || Entrust::can($item['perm']) ))
                    <li class="@if($item['option']['disable'])disabled @endif @if($item['active'])active @endif ">
                        {{-- link --}}
                        @if(!empty($item['route']))
                            <a href="{{ route($item['route']) }}">
                        @elseif(!empty($item['path']))
                            <a href="{{ url($item['path']) }}">
                        @elseif(!empty($item['url']))
                            <a href="{{ url($item['url']) }}" target="_blank">
                        @else
                            <a href="javascript:void(0)">
                        @endif
                        {{-- icon --}}
                        @if(!empty($item['icon']))
                            <span class="{{ $item['icon'] }}" style="margin-right: 10px;"></span>
                        @else
                            <span class="glyphicon glyphicon-none" style="margin-right: 10px;"></span>
                        @endif
                        {{-- text --}}
                        <span>{{ $item['text'] }}</span>
                        {{-- icon of new window --}}
                        @if(!empty($item['route']))
                        @elseif(!empty($item['path']))
                        @elseif(!empty($item['url']))
                            <span class="glyphicon glyphicon-new-window pull-right"></span>
                        @else
                        @endif
                        </a>
                    </li>
                @endif
            @endif
        @endforeach
    @endif
</ul>
