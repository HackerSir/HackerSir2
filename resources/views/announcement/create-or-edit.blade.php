@extends('app')

@section('title', $methodText. '公告')

@section('css')
    {!! HTML::style('css/bootstrap-datetimepicker.css') !!}
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="well">
                    @if(isset($announcement))
                        {!! BootForm::openHorizontal(['sm' => [5, 7], 'lg' => [2, 10]])
                            ->action(route('announcement.update', $announcement))->put() !!}
                        {!! BootForm::bind($announcement) !!}
                    @else
                        {!! BootForm::openHorizontal(['sm' => [5, 7], 'lg' => [2, 10]])
                            ->action(route('announcement.store')) !!}
                    @endif
                    <fieldset>
                        <legend>{{ $methodText }}公告</legend>
                        {!! BootForm::text('標題', 'title')->required() !!}
                        {{-- 開始時間 --}}
                        <div class="form-group has-feedback{{ ($errors->has('start_at'))?' has-error':'' }}">
                            {!! Form::label('start_at', '開始時間', ['class' => 'control-label col-sm-5 col-lg-2']) !!}
                            <div class="col-sm-7 col-lg-10">
                                <div class='input-group date datetimepicker'>
                                    {{-- 由於 model bind 是在 BootForm 上， Form要自行帶入 --}}
                                    {!! Form::text('start_at', isset($announcement) ? $announcement->start_at : null, ['id' => 'start_at', 'placeholder' => 'YYYY-MM-DD HH:mm:ss', 'class' => 'form-control']) !!}
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                </div>
                                @if($errors->has('start_at'))<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>
                                <span class="label label-danger">{{ $errors->first('start_at') }}</span>@endif
                            </div>
                        </div>
                        {{-- 結束時間 --}}
                        <div class="form-group has-feedback{{ ($errors->has('end_at'))?' has-error':'' }}">
                            {!! Form::label('end_at', '結束時間', ['class' => 'control-label col-sm-5 col-lg-2']) !!}
                            <div class="col-sm-7 col-lg-10">
                                <div class='input-group date datetimepicker'>
                                    {{-- 由於 model bind 是在 BootForm 上， Form要自行帶入 --}}
                                    {!! Form::text('end_at', isset($announcement) ? $announcement->end_at : null, ['id' => 'end_at', 'placeholder' => 'YYYY-MM-DD HH:mm:ss', 'class' => 'form-control']) !!}
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                </div>
                                @if($errors->has('end_at'))<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>
                                <span class="label label-danger">{{ $errors->first('end_at') }}</span>@endif
                            </div>
                        </div>
                        {{--公告內容(Markdown) --}}
                        @include('common.markdown', [
                            'name' => 'message',
                            'labelName' => '內容',
                            'text' => isset($announcement) ? $announcement->message : null
                        ])
                        {{-- 提交按鈕 --}}
                        <div class="form-group">
                            <div class="col-sm-offset-5 col-sm-7 col-lg-offset-2 col-lg-10">
                                {!! Form::submit($methodText, ['class' => 'btn btn-primary']) !!}
                                @if(isset($announcement))
                                    <a href="{{ route('announcement.show', $announcement) }}" class="btn btn-default">返回</a>
                                @else
                                    <a href="{{ route('announcement.index') }}" class="btn btn-default">返回</a>
                                @endif
                            </div>
                        </div>
                    </fieldset>
                    {!! BootForm::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    {!! HTML::script('//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js') !!}
    {!! HTML::script('js/moment_zh-tw.js') !!}
    {!! HTML::script('js/bootstrap-datetimepicker.min.js') !!}
    <script type="text/javascript">
        $(function () {
            $('.datetimepicker').each(function () {
                $(this).datetimepicker({
                    format: 'YYYY-MM-DD HH:mm:ss'
                });
            });
        });
    </script>
    @include('common.markdown-js', ['name' => 'message'])
@endsection
