@extends('app')

@section('title', $announcement->title)

@section('content')
    <div class="container">
        <a href="{{ route('announcement.index') }}" class="btn btn-default" role="button"><i class="fa fa-arrow-left"></i> 返回網站公告</a>
        @role('admin')
        <a href="{{ route('announcement.edit', $announcement) }}" class="btn btn-primary" role="button"><i class="fa fa-cogs"></i> 編輯公告</a>
        {!! Form::open(['route' => ['announcement.destroy', $announcement], 'style' => 'display: inline', 'method' => 'DELETE', 'onSubmit' => "return confirm('確定要刪除此公告嗎？');"]) !!}
        <button type="submit" class="btn btn-danger">
            <i class="fa fa-trash"></i> 刪除公告
        </button>
        {!! Form::close() !!}
        @endrole
        <div class="page-header">
            <h1>{{ $announcement->title }}</h1>
        </div>
        <blockquote>
            @if($announcement->user)
            <div class="row">
                <div class="col-md-2">發布者：</div>
                <div><i class="fa fa-pencil"></i>
                    @if(Entrust::hasRole('admin'))
                        {!! link_to_route('user.profile', $announcement->user->getNickname(), $announcement->user) !!}
                    @else
                        {{ $announcement->user->getNickname() }}
                    @endif
                </div>
            </div>
            @endif
            <div class="row">
                <div class="col-md-2">開始時間：</div>
                <div>{{ $announcement->start_at or '（未指定）' }}</div>
            </div>
            <div class="row">
                <div class="col-md-2">結束時間：</div>
                <div>{{ $announcement->end_at or '（未指定）' }}</div>
            </div>
        </blockquote>
        <blockquote>
            {!! App\Helper\MarkdownHelper::translate($announcement->message) !!}
        </blockquote>
    </div>
@endsection
