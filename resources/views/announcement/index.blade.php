@extends('app')

@section('title', '網站公告')

@section('content')
    <div class="container">
        @role('admin')
        <a href="{{ route('announcement.create') }}" class="btn btn-primary"><i class="fa fa-plus-square"></i> 新增公告</a>
        @endrole
        <div class="page-header">
            <h1>網站公告</h1>
        </div>
        <table class="table table-bordered table-hover table-striped">
            <thead>
            <tr>
                <th class="text-center col-md-8">標題</th>
                <th class="text-center col-md-2">開始時間</th>
                <th class="text-center col-md-2">結束時間</th>
            </tr>
            </thead>
            <tbody>
            @foreach($announcements as $announcement)
                <tr>
                    <td>
                        {!! link_to_route('announcement.show', $announcement->title, $announcement) !!}
                    @if($announcement->user)
                        <span class="pull-right"><i class="fa fa-pencil"></i>
                            @if(Entrust::hasRole('admin'))
                                {!! link_to_route('user.profile', $announcement->user->getNickname(), $announcement->user) !!}
                            @else
                                {{ $announcement->user->getNickname() }}
                            @endif
                        </span>
                        @endif
                    </td>
                    <td>{{ $announcement->start_at }}</td>
                    <td>{{ $announcement->end_at }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <div class="text-center">
        {!! $announcements->appends(Request::except(['page']))->render() !!}
    </div>
@endsection
