@extends('app')

@section('title')
    編輯職務
@endsection

@section('css')
    {!! HTML::style('css/bootstrap-datetimepicker.css') !!}
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="well bs-component">
                    {!! Form::open(['route' => ['cadre.update', $cadre->id], 'class' => 'form-horizontal', 'method' => 'PUT']) !!}
                    <fieldset>
                        <legend>編輯職務</legend>
                    </fieldset>
                    <div class="form-group">
                        <label class="control-label col-md-2" for="subject">社員</label>
                        <div class="col-md-9 form-control-static">
                            <img src="{{ Gravatar::src($cadre->member->email, 40) }}" class="img-circle" />
                            <a href="{{ route('member.show',$cadre->member->id) }}">
                                {{ $cadre->member->realname }}
                                @if(!empty($cadre->member->nickname))
                                    （{{ $cadre->member->nickname }}）
                                @endif
                            </a>
                        </div>
                    </div>
                    <div class="form-group has-feedback{{ ($errors->has('title'))?' has-error':'' }}">
                        <label class="control-label col-md-2" for="subject">職稱</label>
                        <div class="col-md-9">
                            {!! Form::text('title', $cadre->title, ['id' => 'title', 'placeholder' => '請輸入職稱', 'class' => 'form-control', 'required']) !!}
                            @if($errors->has('title'))<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>
                            <span class="label label-danger">{{ $errors->first('title') }}</span>@endif
                        </div>
                    </div>
                    <div class="form-group has-feedback{{ ($errors->has('start_at'))?' has-error':'' }}">
                        <label class="control-label col-md-2" for="start_at">開始時間</label>
                        <div class="col-md-9">
                            <div class='input-group date' id='datetimepicker1'>
                                {!! Form::text('start_at', $cadre->start_at, ['id' => 'start_at', 'placeholder' => 'YYYY/MM/DD HH:mm:ss', 'class' => 'form-control']) !!}
                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                            </div>
                            @if($errors->has('start_at'))<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>
                            <span class="label label-danger">{{ $errors->first('start_at') }}</span>@endif
                            <div id="quick-input-start"></div>
                        </div>
                    </div>
                    <div class="form-group has-feedback{{ ($errors->has('end_at'))?' has-error':'' }}">
                        <label class="control-label col-md-2" for="end_at">結束時間</label>
                        <div class="col-md-9">
                            <div class='input-group date' id='datetimepicker2'>
                                {!! Form::text('end_at', $cadre->end_at, ['id' => 'end_at', 'placeholder' => 'YYYY/MM/DD HH:mm:ss', 'class' => 'form-control']) !!}
                                <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                            </div>
                            @if($errors->has('end_at'))<span
                                class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>
                            <span class="label label-danger">{{ $errors->first('end_at') }}</span>@endif
                            <div id="quick-input-end"></div>
                        </div>
                    </div>
                    <div class="form-group has-feedback{{ ($errors->has('comment'))?' has-error':'' }}">
                        <label class="control-label col-md-2" for="subject">評論</label>
                        <div class="col-md-9">
                            {!! Form::textarea('comment', $cadre->comment, ['id' => 'comment', 'placeholder' => '請輸入評論', 'class' => 'form-control']) !!}
                            @if($errors->has('comment'))<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>
                            <span class="label label-danger">{{ $errors->first('comment') }}</span>@endif
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-9 col-md-offset-2">
                            {!! Form::submit('修改職務', ['class' => 'btn btn-primary']) !!}
                            {!! HTML::linkRoute('member.show', '返回', $cadre->member->id, ['class' => 'btn btn-default']) !!}
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    {!! HTML::script('//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js') !!}
    {!! Minify::javascript('/js/moment_zh-tw.js')->withFullUrl() !!}
    {!! HTML::script('js/bootstrap-datetimepicker.min.js') !!}
    <script type="text/javascript">
        $(function () {
            $('#datetimepicker1').datetimepicker({
                format: 'YYYY/MM/DD HH:mm:ss'
            });
            $('#datetimepicker2').datetimepicker({
                format: 'YYYY/MM/DD HH:mm:ss'
            });
        });

        $(function () {
            startYear = 103;
            yearCount = 5;
            btnBar = '';
            for (i = 0; i < yearCount; i++) {
                temp = '<a href="javascript:void(0)" class="btn btn-default btn-xs" data-year="' + (startYear + i) + '" data-term="1">' + (startYear + i) + '上</a> ';
                btnBar += temp;
            }
            btnBar += '<br />';
            for (i = 0; i < yearCount; i++) {
                temp = '<a href="javascript:void(0)" class="btn btn-default btn-xs" data-year="' + (startYear + i) + '" data-term="2">' + (startYear + i) + '下</a> ';
                btnBar += temp;
            }
            $('#quick-input-start').html(btnBar);
            $('#quick-input-end').html(btnBar);
        });

        $(document).on('click', '#quick-input-start a', function () {
            year = $(this).data('year') + 1911;
            if ($(this).data('term') == 1) {
                startAt = year + '/08/01 00:00:00';
            } else {
                startAt = (year + 1) + '/02/01 00:00:00';
            }
            $('#start_at').val(startAt);
        });

        $(document).on('click', '#quick-input-end a', function () {
            year = $(this).data('year') + 1911;
            if ($(this).data('term') == 1) {
                endAt = (year + 1) + '/01/31 23:59:59';
            } else {
                endAt = (year + 1) + '/07/31 23:59:59';
            }
            $('#end_at').val(endAt);
        });
    </script>
@endsection
