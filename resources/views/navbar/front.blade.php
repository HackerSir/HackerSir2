<nav class="navbar navbar-default navbar-fixed-top">
    <div class="navbar-head">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                data-target="#navbar-menu">
            <span class="sr-only">Menu</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="{{ route('home') }}">{{ Config::get('site.name') }}</a>
    </div>
    <div class="collapse navbar-collapse" id="navber-menu">
        <ul class="nav navbar-nav">
            <li class="dropdown">
                <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">關於我們About <span class="caret"></span></a>
                <ul class="dropdown-menu" role="menu">
                    <li><a href="{!! route('about.club') !!}">社團介紹Club</a></li>
                    {{--<li><a href="{!! route('about.cadre') !!}">現任幹部Cadre</a></li>--}}
                    <li><a href="{!! route('about.rule') !!}">社團規章Rule</a></li>
                    <li><a href="{!! route('about.friend') !!}">合作夥伴Friend</a></li>
                    {{--<li><a href="{!! route('about.milestone') !!}">里程碑Milestone</a></li>--}}
                </ul>
            </li>
            <li class="dropdown">
                <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">社團課程Lesson <span class="caret"></span></a>
                <ul class="dropdown-menu" role="menu">
                    <li><a href="{!! route('lesson.regular') !!}">定期社課Regular</a></li>
                    <li><a href="{!! route('lesson.cooperation') !!}">合作社課Cooperation</a></li>
{{--                    <li><a href="{!! route('lesson.special') !!}">主題課程Special</a></li>--}}
                </ul>
            </li>
            <li><a href="http://hackersir.kktix.cc" target="_blank">社團活動Activity</a></li>
        </ul>
    </div>
</nav>
