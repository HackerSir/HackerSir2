<nav class="navbar navbar-default">
    <div class="navbar-head">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                data-target="#menu-navbar">
            <span class="sr-only">Menu</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
    </div>
    <div class="collapse navbar-collapse" id="menu-navbar">
        <ul class="nav navbar-nav">
            <li class="item"><a href="#origin">起源 Origin</a></li>
            <li class="item"><a href="#target">目標 Target</a></li>
            <li class="item"><a href="#lesson">課程 Lesson</a></li>
            <li class="item"><a href="#activity">活動 Activity</a></li>
            <li class="item"><a href="#join">加入我們 Join us!</a></li>
        </ul>
    </div>
</nav>
