@extends('app')

@section('title')
    信箱清單
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="panel panel-default">
                    <div class="panel-heading">信箱清單</div>
                    <div class="panel-body">
                        <table class="table table-hover">
                            @foreach(Auth::user()->userMails as $userMail)
                                <tr>
                                    <td style="vertical-align: middle">
                                        {{ $userMail->email }}
                                        @if($userMail->isPrimary())
                                            <span class="label label-info">主要</span>
                                        @endif
                                        @if($userMail->isConfirmed())
                                            <span class="label label-success">已驗證</span>
                                        @else
                                            <a href="{{ URL::route('user.resend', $userMail->email) }}" title="點此重新發送驗證信"><span class="label label-danger">未驗證</span></a>
                                        @endif
                                    </td>
                                    <td class="text-right">
                                        {!! Form::open(['route' => ['user-mail.destroy'], 'style' => 'display: inline', 'method' => 'DELETE', 'onSubmit' => "return confirm('確定要刪除此信箱嗎？\\n（重新添加仍須重新驗證）');"]) !!}
                                        {!! Form::hidden('email', $userMail->email) !!}
                                        {!! Form::submit('刪除', ['class' => 'btn btn-danger']) !!}
                                        {!! Form::close() !!}
                                    </td>
                                </tr>
                            @endforeach
                            {!! Form::open(['route' => ['user-mail.store'], 'class' => 'form-inline']) !!}
                                <tr>
                                    <td>
                                        <div class="form-group has-feedback{{ ($errors->has('nickname'))?' has-error':'' }}">
                                            {!! Form::email('email', null, ['id' => 'email', 'placeholder' => '請輸入欲新增之信箱', 'class' => 'form-control', 'required']) !!}
                                            @if($errors->has('email'))<span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>
                                            <span class="label label-danger">{{ $errors->first('email') }}</span>@endif
                                        </div>
                                    </td>
                                    <td class="text-right">
                                        {!! Form::submit('新增', ['class' => 'btn btn-danger']) !!}
                                    </td>
                                </tr>
                            {!! Form::close() !!}
                        </table>
                        <div class="text-center">
                            {!! HTML::linkRoute('user.profile', '返回個人資料', [], ['class' => 'btn btn-default']) !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
