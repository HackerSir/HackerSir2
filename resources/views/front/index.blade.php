@extends('app')

@section('title') 首頁 @endsection

@section('css')
    <style>
        body {
            margin: 0;
            font-size: 20px;
        }

        section#header {
            height: 100vh;
            background-size: 100%;
            background: url("{!! asset('img/index-header.jpg') !!}") no-repeat;
        }

        section#header img:first-child {
            -webkit-filter: drop-shadow(0px 0px 30px white);
            filter:         drop-shadow(0px 0px 30px white);
            -webkit-animation-duration: 1.5s;
        }

        section#header div.flash {
            -webkit-animation-duration: 2s;
            -webkit-animation-delay: 1s;
            -webkit-animation-iteration-count: infinite;
        }

        div.supershape {
            height: 100px;
            width: 0;
            padding: 0;
            border-left: 50px solid #B7777F;
            border-right: 50px solid #B7777F;
            border-bottom: 25px solid transparent;
        }

        span.supershape {
            transform: translate(-50%, 50%);
            -webkit-filter: drop-shadow(0px 0px 30px white);
            filter:         drop-shadow(0px 0px 30px white);
        }

        section#join > div.container > div.row > div+div {
            border-left: 1px solid darkseagreen;
            min-height: 90vh;
        }

        section#join > div.container > div.row > div.col-sm-6 > a.btn{
            color: white;
        }
        section#join > div.container > div.row > div.col-sm-6 > a.btn:hover{
            color: lightblue;
        }

        hr {
            border-style: solid;
            border-color: indianred;
            border-width: 1px 0 0 0;
            border-radius: 25px;
        }

        section#activity > div.container-fluid > div.row >  div+div {
            border-left: 1px solid indianred;
            min-height: 30vh;
        }

        p {
            text-indent: 2em;
        }


    </style>
@endsection

@section('header')
    <section class="text-center" id="header">
        {{-- 黑帽Logo --}}
        <img src="{{ asset('img/logo/HackerSir.png') }}" alt="HackerSir" class="animated slideInUp" style="height: 50%"/><br/>
        {{-- 文字Logo --}}
        <img src="{{ asset('img/logo/HackerSir_Text.png') }}" alt="HackerSir" class="animated slideInUp" style="max-width: 100%; max-height: 35%"/>
        {{-- Scroll Down --}}
        <div class="text-light place-bottom animated flash">
            <a href="#origin" style="text-decoration: none; color: #f0f0f0;">
                <h3>START</h3>
                <i class="fa fa-angle-double-down fa-5x"></i>
            </a>
        </div>
    </section>
@endsection

@section('content')
    {{-- Origin --}}
    <section id="origin" class="green">
        <div class="container">
            <p><strong class="lead">黑客社</strong>是由一群喜歡資訊安全的人們所創立，因緣際會之下成立了這個社團。</p>
            <br>
            <p>我們致力推廣「<strong>資訊安全</strong>」及「<strong>程式設計</strong>」，秉持著「創新與實作」的理念， 除了推廣資訊安全的基礎概念、程式設計普及化、開源文化，同時也致力於推廣包括電子簽到、電子投票等等，
                希望可以讓每位社員甚至每位學生都能了解到資安的重要性及體驗到更加便利、有趣的未來！</p>
            <br>
            <br>
            <p>如果你有興趣，歡迎戳一下下面的按鈕，了解更多關於黑客社的故事。</p>
            <br>
            <div class="text-center">
                {!! HTML::linkRoute('about.club', '風起雲湧－關於黑客社', [], ['type'=> 'button', 'class' => 'btn btn-lg btn-primary']) !!}
            </div>
        </div>
    </section>

    {{-- target --}}
    <section id="target" class="grey">
        <div class="container text-center">
            <h2>兩大目標</h2>

            <div class="row">
                <div class="col-sm-6">
                    {{-- 徽章 --}}
                    <div class="container supershape">
                        <span class="fa fa-key fa-2x supershape"></span>
                    </div>
                    {{-- 說明 --}}
                    <h3>資訊安全</h3>
                    <br>
                </div>
                <div class="col-sm-6">
                    {{-- 徽章 --}}
                    <div class="container supershape">
                        <span class="fa fa-cogs fa-2x supershape"></span>
                    </div>
                    {{-- 說明 --}}
                    <h3>黑客精神</h3>
                    <br>
                </div>
            </div>
            <br />
            <h2 class="text-center">四大支線</h2>
            <div class="row">
                <div class="col-sm-3">
                    {{-- 徽章 --}}
                    <div class="container supershape">
                        <span class="fa fa-lock fa-2x supershape"></span>
                    </div>
                    {{-- 說明 --}}
                    <h3>推廣資安概念</h3>
                    <br>
                </div>
                <div class="col-sm-3">
                    {{-- 徽章 --}}
                    <div class="container supershape">
                        <span class="fa fa-terminal fa-2x supershape"></span>
                    </div>
                    {{-- 說明 --}}
                    <h3>程式設計普及化</h3>
                    <br>
                </div>
                <div class="col-sm-3">
                    {{-- 徽章 --}}
                    <div class="container supershape">
                        <span class="fa fa-code fa-2x supershape"></span>
                    </div>
                    {{-- 說明 --}}
                    <h3>提倡開放原始碼</h3>
                    <br>
                </div>
                <div class="col-sm-3">
                    {{-- 徽章 --}}
                    <div class="container supershape">
                        <span class="fa fa-shield fa-2x supershape"></span>
                    </div>
                    {{-- 說明 --}}
                    <h3>建立安全的E化校園</h3>
                    <br>
                </div>
            </div>
        </div>
    </section>

    {{-- Lesson --}}
    <section id="lesson" class="green">
        <div class="container">
            <h2 class="text-center">程式設計課程</h2>
            <hr>
            <p>這學期我們承襲上學期的課程，依舊是以 <strong>Python</strong> 作為教學的語言，並且，我們記取了上學期的點點滴滴，重新編寫了教學課綱，希望可以讓社員在最短的時間內學會Python。</p>
            <br>
            <div class="text-center">
                {!! link_to_route('lesson.regular', '我要學Python', [], ['type'=> 'button', 'class' => 'btn btn-lg btn-info']) !!}
            </div>
            <br>
            <h2 class="text-center">資訊安全課程</h2>
            <hr>
            <p>隨著資訊化時代的到來，如何在網路世界中保護自己更為重要，為了提升學生們的資安意識，我們會介紹各種資安觀念，伴隨者 <strong>CTF解題活動</strong> 讓大家了解各種黑客常用技巧以進行防範。</p>
            <br>
            <div class="text-center">
                {!! link_to_route('lesson.cooperation', '我要解CTF', [], ['type'=> 'button', 'class' => 'btn btn-lg btn-danger disabled']) !!}
            </div>
            <br>
            <h2 class="text-center">隱藏課程</h2>
            <hr>
            <p class="text-center">偶爾我們可能會有神秘隱藏小社課，歡迎關注我們臉書社團。</p>
            <br>
            <div class="text-center">
                {!! link_to('https://www.facebook.com/groups/HackerSir/', '加關注!!', ['target' => '_blank', 'type'=> 'button', 'class' => 'btn btn-lg btn-info']) !!}
            </div>
        </div>
    </section>

    {{-- Activity --}}
    <section id="activity" class="grey">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-4">
                    <h2 class="text-center">SITCON x HackerSir</h2>
                    <h3 class="text-center">台中定期聚</h3>
                    <br>
                    <p>我們和SITCON及興大電資同好會將一起舉辦台中定期聚，單數月由我們的合作夥伴「興大電資同好會」舉辦，雙數月則是由我們來舉辦，在定期聚裡，我們可以認識來自不同領域卻對資訊有研究的人們，互相切磋、互相琢磨，增進自己的見聞與技藝！</p>
                    <br>
                </div>
                <div class="col-sm-4">
                    <h2 class="text-center">HackerSir x FISA</h2>
                    <h3 class="text-center">期末/不定期聚餐</h3>
                    <br>
                    <p>我們和FISA是群同根生的好夥伴，因此我們會常常舉辦聚餐，定期聚餐部分有期末聚餐，而偶爾也有可能會出現緊急降臨的聚餐活動，只要是兩邊的社員都可以參加喔！</p>
                    <br>
                </div>
                <div class="col-sm-4">
                    <h2 class="text-center">中區Workshop計畫</h2>
                    <h3 class="text-center">Workshop</h3>
                    <br>
                    <p>動手做！動手玩！讓我們用一點點的時間教些基礎，剩下時間就交由各位動手玩創意，自由發揮、自由操作。</p>
                </div>
            </div>
        </div>
    </section>

    {{-- Join us --}}
    <section id="join" class="red">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 text-center">
                    <h2>黑客社聯絡方式</h2>
                    <br>
                    <a href="https://www.facebook.com/HackerSir.tw" class="btn btn-lg" target="_blank"><span class="fa fa-facebook-square fa-2x"></span> 粉絲專頁</a><br>
                    <a href="https://www.facebook.com/groups/HackerSir/" class="btn btn-lg" target="_blank"><span class="fa fa-facebook-square fa-2x"></span> 公開社團</a><br>
                    <a href="https://github.com/HackerSir" class="btn btn-lg" target="_blank"><span class="fa fa-github fa-2x"></span> GitHub</a><br>
                    <a href="javascript:void(0)" onclick="javascript:alert('口卡！口卡！目前沒有影片！')" class="btn btn-lg" target="_blank"><span class="fa fa-youtube-square fa-2x"></span> 活動直播</a>
                    <br>
                </div>
                <div class="col-sm-6">
                    <h2 class="text-center">如果你有以下任一特質：</h2>
                    <br>
                    <ul class="list-unstyled" style="margin-left: 12rem">
                        <li><span class="fa fa-check-square-o"></span> 喜愛新事物</li>
                        <li><span class="fa fa-check-square-o"></span> 對資安有興趣</li>
                        <li><span class="fa fa-check-square-o"></span> 喜歡Coding</li>
                        <li><span class="fa fa-check-square-o"></span> 想要改變世界從自己開始</li>
                        <li><span class="fa fa-check-square-o"></span> 想當工程師</li>
                        <li><span class="fa fa-check-square-o"></span> 有新鮮強壯的肝</li>
                    </ul>
                    <br>
                    <h3 class="text-center">你可以......</h3>
                    <br>
                    <div class="text-center">
                        <a href="javascript:void(0)" onclick="javascript:alert('對不起，目前暫時不開放線上申請，請直接找幹部填入社單。\n你可以在社課、社內活動期間找到他們！')" type="button" class="btn btn-lg btn-primary disabled">加入我們！</a>
                    </div>
                </div>
            </div>
            <br><br><br>
        </div>
    </section>
@endsection

@section('js')
    <script>
        var elementHeight = [$('section#header').outerHeight() + $('nav').outerHeight() - 40];
        elementHeight.push(elementHeight[0] + $('section#origin').outerHeight());
        elementHeight.push(elementHeight[1] + $('section#target').outerHeight());
        elementHeight.push(elementHeight[2] + $('section#lesson').outerHeight());
        elementHeight.push(elementHeight[3] + $('section#activity').outerHeight());
        elementHeight.push(elementHeight[4] + $('section#join').outerHeight());

        var navItems =  $('nav>#menu-navbar>ul>li');

        var resetNavbarActive = function(){
            $(navItems).each(function(){
                $(this).removeClass('active');
            });
        };

        $(window).scroll(function() {
            if ($(document).scrollTop() >= $('#header').outerHeight()) {
                $('nav').addClass('navbar-fixed-top');
                $("body").addClass('fixed');
            } else {
                $('nav').removeClass('navbar-fixed-top');
                $("body").removeClass('fixed');
            }

            resetNavbarActive();
            if ($(document).scrollTop() >= elementHeight[0] && $(document).scrollTop() < elementHeight[1]) {
                $(navItems[0]).addClass('active');
            } else if ($(document).scrollTop() >= elementHeight[1] && $(document).scrollTop() < elementHeight[2]) {
                $(navItems[1]).addClass('active');
            } else if ($(document).scrollTop() >= elementHeight[2] && $(document).scrollTop() < elementHeight[3]) {
                $(navItems[2]).addClass('active');
            } else if ($(document).scrollTop() >= elementHeight[3] && $(document).scrollTop() < elementHeight[4]) {
                $(navItems[3]).addClass('active');
            } else if ($(document).scrollTop() >= elementHeight[4]) {
                $(navItems[4]).addClass('active');
            }
        });
    </script>
    <script>
        $('a[href*=#]:not([href=#])').click(function () {
            console.log(location.pathname);
            if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                if (target.length) {
                    $('html,body').animate({
                        scrollTop: (target.offset().top - 40 + 1) // adjust this according to your content
                    }, 1000);
                    return false;
                }
            }
        });
    </script>
    @if(Carbon::now()->gte(Carbon::createFromDate(Carbon::now()->year, 12, 20, 'Asia/Taipei')) and Carbon::now()->lte(Carbon::createFromDate(Carbon::now()->year, 12, 30, 'Asia/Taipei')) and !preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i',$_SERVER['HTTP_USER_AGENT']) or preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($_SERVER['HTTP_USER_AGENT'],0,4)) )
        {!! HTML::script('js/snow.js') !!}
        <script>
            $(document).ready( function(){
                $.fn.snow({minSize: 5, maxSize: 50});
            });
        </script>
    @endif
@endsection
