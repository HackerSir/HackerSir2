@extends('app')

@section('title')
    合作課程
@endsection

@section('css')
    <style>
        ul > li {
            font-size: 13pt;
        };
    </style>
@endsection

@section('content')
    <div class="container">
        <p class="lead">有時候，我們會和其他的單位推出合作課程，只要是我們社員都可以參加！</p>
        <div class="row">
            <div class="col-sm6">
                <div class="row">
                    <div class="col-sm-6">
                        {{-- 課程圖片 --}}
                        {!! HTML::image('img/security.jpg', 'Information Security', ['class' => 'img-responsive img-rounded']) !!}
                    </div>
                    <div class="col-sm-6">
                        {{-- 課程名稱 --}}
                        <h3>資訊安全</h3>
                        <dl>
                            <dt>合作單位</dt>
                            <dd>逢甲大學資訊安全策進會</dd>
                            <dt>時間</dt>
                            <dd>每個星期五 下午三點至六點<strong>（不含國定假日、期中期末考周及前一周）</strong></dd>
                            <dt>地點</dt>
                            <dd>資電235</dd>
                            <dt>資格</dt>
                            <dd>無、只要有心都可以</dd>
                            <dt>課表</dt>
                            <dd>{!! link_to('http://fisa.hackersir.org', 'FISA網站', ['target' => '_blank']) !!}</dd>
                        </dl>
                    </div>
                </div>
                <blockquote class="text-center">資安與技術交織而成的社課！</blockquote>
            </div>
        </div>
    </div>
@endsection
