@extends('app')

@section('title')
    定期課程
@endsection

@section('css')
    <style>
        ul > li {
            font-size: 13pt;
        }
    </style>
@endsection

@section('content')
    <div class="container">
        <p class="lead">每個學期我們都會有一至二堂定期的社團課程，本學期的定期課程如下：</p>

        <div class="row">
            <div class="col-sm-12">
                <div class="row">
                    <div class="col-sm-6">
                        {{-- 課程圖片 --}}
                        {!! HTML::image('img/python.jpg', 'Python', ['class' => 'img-responsive img-rounded']) !!}
                    </div>
                    <div class="col-sm-6">
                        {{-- 課程名稱 --}}
                        <h2>Python 101</h2>
                        <dl>
                            <dt>時間</dt>
                            <dd>每個星期二 晚上七點至九點<strong>（不含國定假日、期中期末考周及前一周）</strong></dd>
                            <dt>地點</dt>
                            <dd>每周於Facebook社團公告</dd>
                            <dt>資格</dt>
                            <dd>無、只要有心都可以</dd>
                        </dl>
                    </div>
                </div>
                <blockquote class="text-center">沒有技術力？那麼從Python 101開始就對了！</blockquote>
            </div>
        </div>
    </div>
@endsection
