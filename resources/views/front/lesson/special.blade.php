@extends('app')

@section('title')
    主題課程
@endsection

@section('header')

@endsection

@section('content')
    <div class="container">
        <p>本學期預定舉辦下列主題課程，歡迎各位社員隨時捧場！</p>
        <div class="row">
            <div class="row">
                <div class="col-sm-6">
                    {{-- 課程圖片 --}}
                </div>
                <div class="col-sm-6">
                    <h3>Git Workshop</h3>
                    <ul class="list-unstyled">
                        <li>時間：</li>
                        <li>地點：</li>
                        <li>資格：</li>
                        <li>注意：</li>
                    </ul>
                </div>
            </div>
            <backquote>課程簡介</backquote>
        </div>
    </div>
@endsection
