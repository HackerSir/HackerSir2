@extends('app')

@section('title')
    合作夥伴/友社
@endsection

@section('css')
    <style>
        hr.style-seven {
            height: 30px;
            border-style: solid;
            border-color: black;
            border-width: 1px 0 0 0;
            border-radius: 20px;
        }

        img.logo {
            max-height: 100%;
            max-width: 100%;
        }
    </style>
@endsection

@section('header')

@endsection

@section('content')
    <div class="container">
        <p class="lead">我們有許許多多的合作夥伴，他們來自世界各地，四面八方。感謝有他們，和我們並肩走來！</p>

        <h2 class="text-center">校內組織</h2>
        <hr class="style-seven">
        <div class="row">
            <div class="col-sm-6">
                <div class="row">
                    <div class="col-sm-6">
                        <a href="http://fisa.hackersir.org" target="_blank"><img src="{{ asset('img/logo/FISA.png') }}" alt="FISA"
                                                              class="logo"/></a>
                    </div>
                    <div class="col-sm-6">
                        <h3>FISA</h3>
                        <h4>逢甲大學資訊安全策進會</h4>

                        <p>
                            FISA資策會源自於遠古學長們的傳承，最初由李維斌老師所召集。<br/>
                            <br/>
                            我們希望從學生時期就培養資安觀念，
                            除了在校園中宣導基本的資安觀念，
                            我們也提供想玩技術的夥伴們空間，
                            讓更多關注資安的駭客們，
                            有合法的環境，可以更深入的研究資安相關議題。</p>
                    </div>
                </div>
            </div>

        </div>

        <h2 class="text-center">社團</h2>
        <hr class="style-seven">
        <div class="row">
            <div class="col-sm-6">
                <div class="row">
                    <div class="col-sm-6">
                        <a href="http://nchuit.cc" target="_blank"><img src="{!! asset('img/logo/NCHUIT.png') !!}" alt="NCHUIT" class="logo"></a>
                    </div>
                    <div class="col-sm-6">
                        <h3>NCHUIT</h3>
                        <h4>中興大學資訊科學研習社</h4>

                        <p>推廣興大的資訊風氣，使興大有更多能夠了解並且使用資訊科技的同學們，並且能夠透過社團互相交流、取暖(?)</p>
                    </div>
                </div>
            </div>
        </div>

        <h2 class="text-center">校外組織</h2>
        <hr class="style-seven">
        <div class="row">
            <div class="col-sm-6">
                <div class="row">
                    <div class="col-sm-6">
                        <a href="http://sitcon.org" target="_blank"><img src="{!! asset('img/logo/sitcon.png') !!}" alt="SITCON 學生計算機年會"
                                                         class="logo"/></a>
                    </div>
                    <div class="col-sm-6">
                        <h3>SITCON</h3>
                        <h4>學生計算機年會</h4>

                        <p>SITCON
                            學生計算機年會，由對資訊抱持熱誠的學生們所組成的同名社群自發籌辦，希望能透過技術與知識的激盪，給予學生們一個用自身力量實踐夢想的舞台。本屆年會之主軸聚焦於真正的駭客精神，The
                            True Hackers 。希望讓大家瞭解駭客精神的中心思想，以及駭客精神在各個領域的展現。</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
