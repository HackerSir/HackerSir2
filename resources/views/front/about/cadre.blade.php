@extends('app')

@section('title')
    現任幹部介紹
@endsection

@section('header')

@endsection

@section('content')
    <div class="container">
        <h1 class="text-center">社長</h1>
        <hr>
        <div class="row">
            <div class="col-sm-6">
                <div class="row">
                    <div class="col-sm-6">
                        <img src="{{ URL::asset('img/avatar_nopic.png') }}" alt="No Picture"
                             class="img-responsive img-circle">
                    </div>
                    <div class="col-sm-6">
                        <h2>侯均靜</h2>
                        <ul class="list-unstyled">
                            <li>第一項公開資料</li>
                            <li>第二項公開資料</li>
                            <li>第三項公開資料</li>
                        </ul>
                    </div>
                </div>
                <blockquote>名言佳句/警世名言/我要豪洨/尼瑪廢話</blockquote>
            </div>
        </div>

        <h1 class="text-center">副社長</h1>
        <hr>
        <div class="row">
            <div class="col-sm-6">
                <div class="row">
                    <div class="col-sm-6">
                        <img src="{{ URL::asset('img/avatar_nopic.png') }}" alt="No Picture"
                             class="img-responsive img-circle">
                    </div>
                    <div class="col-sm-6">
                        <h2>陳靖德</h2>
                        <ul class="list-unstyled">
                            <li>第一項公開資料</li>
                            <li>第二項公開資料</li>
                            <li>第三項公開資料</li>
                        </ul>
                    </div>
                </div>
                <blockquote>名言佳句/警世名言/我要豪洨/尼瑪廢話</blockquote>
            </div>
            <div class="col-sm-6">
                <div class="row">
                    <div class="col-sm-6">
                        <img src="{{ URL::asset('img/avatar_nopic.png') }}" alt="No Picture"
                             class="img-responsive img-circle">
                    </div>
                    <div class="col-sm-6">
                        <h2>楊易哲</h2>
                        <ul class="list-unstyled">
                            <li>第一項公開資料</li>
                            <li>第二項公開資料</li>
                            <li>第三項公開資料</li>
                        </ul>
                    </div>
                </div>
                <blockquote>名言佳句/警世名言/我要豪洨/尼瑪廢話</blockquote>
            </div>
        </div>

        <h1 class="text-center">學術</h1>
        <hr>
        <div class="row">
            <div class="col-sm-6">
                <div class="row">
                    <div class="col-sm-6">
                        <img src="{{ URL::asset('img/avatar_nopic.png') }}" alt="No Picture"
                             class="img-responsive img-circle">
                    </div>
                    <div class="col-sm-6">
                        <h2>陳昱亨</h2>
                        <ul class="list-unstyled">
                            <li>第一項公開資料</li>
                            <li>第二項公開資料</li>
                            <li>第三項公開資料</li>
                        </ul>
                    </div>
                </div>
                <blockquote>名言佳句/警世名言/我要豪洨/尼瑪廢話</blockquote>
            </div>
        </div>

        <h1 class="text-center">總務</h1>
        <hr>
        <div class="row">
            <div class="col-sm-6">
                <div class="row">
                    <div class="col-sm-6">
                        <img src="{{ URL::asset('img/avatar_nopic.png') }}" alt="No Picture"
                             class="img-responsive img-circle">
                    </div>
                    <div class="col-sm-6">
                        <h2>林柏丞</h2>
                        <ul class="list-unstyled">
                            <li>第一項公開資料</li>
                            <li>第二項公開資料</li>
                            <li>第三項公開資料</li>
                        </ul>
                    </div>
                </div>
                <blockquote>名言佳句/警世名言/我要豪洨/尼瑪廢話</blockquote>
            </div>
            <div class="col-sm-6">
                <div class="row">
                    <div class="col-sm-6">
                        <img src="{{ URL::asset('img/avatar_nopic.png') }}" alt="No Picture"
                             class="img-responsive img-circle">
                    </div>
                    <div class="col-sm-6">
                        <h2>顏郁修</h2>
                        <ul class="list-unstyled">
                            <li>第一項公開資料</li>
                            <li>第二項公開資料</li>
                            <li>第三項公開資料</li>
                        </ul>
                    </div>
                </div>
                <blockquote>名言佳句/警世名言/我要豪洨/尼瑪廢話</blockquote>
            </div>
        </div>

        <h1 class="text-center">公關</h1>
        <hr>
        <div class="row">
            <div class="col-sm-6">
                <div class="row">
                    <div class="col-sm-6">
                        <img src="{{ URL::asset('img/avatar_nopic.png') }}" alt="No Picture"
                             class="img-responsive img-circle">
                    </div>
                    <div class="col-sm-6">
                        <h2>王維綸</h2>
                        <ul class="list-unstyled">
                            <li>第一項公開資料</li>
                            <li>第二項公開資料</li>
                            <li>第三項公開資料</li>
                        </ul>
                    </div>
                </div>
                <blockquote>名言佳句/警世名言/我要豪洨/尼瑪廢話</blockquote>
            </div>
        </div>

        <h1 class="text-center">網管</h1>
        <hr>
        <div class="row">
            <div class="col-sm-6">
                <div class="row">
                    <div class="col-sm-6">
                        <img src="{{ URL::asset('img/avatar_nopic.png') }}" alt="No Picture"
                             class="img-responsive img-circle">
                    </div>
                    <div class="col-sm-6">
                        <h2>張辰德</h2>
                        <ul class="list-unstyled">
                            <li>第一項公開資料</li>
                            <li>第二項公開資料</li>
                            <li>第三項公開資料</li>
                        </ul>
                    </div>
                </div>
                <blockquote>名言佳句/警世名言/我要豪洨/尼瑪廢話</blockquote>
            </div>
            <div class="col-sm-6">
                <div class="row">
                    <div class="col-sm-6">
                        <img src="{{ URL::asset('img/avatar_nopic.png') }}" alt="No Picture"
                             class="img-responsive img-circle">
                    </div>
                    <div class="col-sm-6">
                        <h2>黃仁又</h2>
                        <ul class="list-unstyled">
                            <li>第一項公開資料</li>
                            <li>第二項公開資料</li>
                            <li>第三項公開資料</li>
                        </ul>
                    </div>
                </div>
                <blockquote>名言佳句/警世名言/我要豪洨/尼瑪廢話</blockquote>
            </div>
        </div>

        <h1 class="text-center">文書</h1>
        <hr>
        <div class="row">
            <div class="col-sm-6">
                <div class="row">
                    <div class="col-sm-6">
                        <img src="{{ URL::asset('img/avatar_nopic.png') }}" alt="No Picture"
                             class="img-responsive img-circle">
                    </div>
                    <div class="col-sm-6">
                        <h2>蔡秉辰</h2>
                        <ul class="list-unstyled">
                            <li>第一項公開資料</li>
                            <li>第二項公開資料</li>
                            <li>第三項公開資料</li>
                        </ul>
                    </div>
                </div>
                <blockquote>名言佳句/警世名言/我要豪洨/尼瑪廢話</blockquote>
            </div>
        </div>

        <h1 class="text-center">美宣</h1>
        <hr>
        <div class="row">
            <div class="col-sm-6">
                <div class="row">
                    <div class="col-sm-6">
                        <img src="{{ URL::asset('img/avatar_nopic.png') }}" alt="No Picture"
                             class="img-responsive img-circle">
                    </div>
                    <div class="col-sm-6">
                        <h2>林倚婕</h2>
                        <ul class="list-unstyled">
                            <li>第一項公開資料</li>
                            <li>第二項公開資料</li>
                            <li>第三項公開資料</li>
                        </ul>
                    </div>
                </div>
                <blockquote>名言佳句/警世名言/我要豪洨/尼瑪廢話</blockquote>
            </div>
        </div>

        <h1 class="text-center">顧問</h1>
        <hr>
        <div class="row">
            <div class="col-sm-6">
                <div class="row">
                    <div class="col-sm-6">
                        <img src="{{ URL::asset('img/avatar_nopic.png') }}" alt="No Picture"
                             class="img-responsive img-circle">
                    </div>
                    <div class="col-sm-6">
                        <h2>許展源</h2>
                        <ul class="list-unstyled">
                            <li>第一項公開資料</li>
                            <li>第二項公開資料</li>
                            <li>第三項公開資料</li>
                        </ul>
                    </div>
                </div>
                <blockquote>名言佳句/警世名言/我要豪洨/尼瑪廢話</blockquote>
            </div>
            <div class="col-sm-6">
                <div class="row">
                    <div class="col-sm-6">
                        <img src="{{ URL::asset('img/avatar_nopic.png') }}" alt="No Picture"
                             class="img-responsive img-circle">
                    </div>
                    <div class="col-sm-6">
                        <h2>陳俊佑</h2>
                        <ul class="list-unstyled">
                            <li>第一項公開資料</li>
                            <li>第二項公開資料</li>
                            <li>第三項公開資料</li>
                        </ul>
                    </div>
                </div>
                <blockquote>名言佳句/警世名言/我要豪洨/尼瑪廢話</blockquote>
            </div>
            <div class="col-sm-6">
                <div class="row">
                    <div class="col-sm-6">
                        <img src="{{ URL::asset('img/avatar_nopic.png') }}" alt="No Picture"
                             class="img-responsive img-circle">
                    </div>
                    <div class="col-sm-6">
                        <h2>呂銘洋</h2>
                        <ul class="list-unstyled">
                            <li>第一項公開資料</li>
                            <li>第二項公開資料</li>
                            <li>第三項公開資料</li>
                        </ul>
                    </div>
                </div>
                <blockquote>名言佳句/警世名言/我要豪洨/尼瑪廢話</blockquote>
            </div>
            <div class="col-sm-6">
                <div class="row">
                    <div class="col-sm-6">
                        <img src="{{ URL::asset('img/avatar_nopic.png') }}" alt="No Picture"
                             class="img-responsive img-circle">
                    </div>
                    <div class="col-sm-6">
                        <h2>荊輔翔</h2>
                        <ul class="list-unstyled">
                            <li>第一項公開資料</li>
                            <li>第二項公開資料</li>
                            <li>第三項公開資料</li>
                        </ul>
                    </div>
                </div>
                <blockquote>名言佳句/警世名言/我要豪洨/尼瑪廢話</blockquote>
            </div>
        </div>
    </div>
@endsection
